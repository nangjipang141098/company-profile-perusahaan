-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2021 at 04:11 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banggaso_nindya`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_user` varchar(25) NOT NULL,
  `admin_pass` varchar(50) NOT NULL,
  `admin_nama` varchar(30) NOT NULL,
  `admin_alamat` varchar(250) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_telepon` varchar(15) NOT NULL,
  `admin_ip` varchar(12) DEFAULT NULL,
  `admin_online` int(10) DEFAULT NULL,
  `admin_level_kode` int(3) NOT NULL,
  `admin_status` char(1) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_user`, `admin_pass`, `admin_nama`, `admin_alamat`, `admin_email`, `admin_telepon`, `admin_ip`, `admin_online`, `admin_level_kode`, `admin_status`, `foto`) VALUES
('qqq', '', 'qqq', '', 'qqq', 'qqq', NULL, NULL, 0, 'N', '6ca8570d342e7247d23e07f12df55c74.png'),
('reza', 'reza', 'rezaa', 'dd', 'ddd', '1122', NULL, NULL, 1, 'A', 'b7e39bd491fa2e68f336d24b6aa33b9f.png'),
('s', '', 's', '', 's', 's', NULL, NULL, 0, 'A', '8a715dffb61acf95cc7c68dc6877b000.jpg'),
('www', '', 'www', '', 'wwww', '1122', NULL, NULL, 0, 'A', '952c93f546a7d9e0320048e81c909e83.png'),
('wwwq', '', 'www', '', 'ww', 'ff', NULL, NULL, 0, 'A', '8cbb529a82968969fae2135734d8ebc8.png'),
('wwwqww', '', 'www', '', 'wwww', 'w', NULL, NULL, 2, 'A', 'a00088f166c9f63233e44ab63fcbc8c9.png');

-- --------------------------------------------------------

--
-- Table structure for table `admin_level`
--

CREATE TABLE `admin_level` (
  `admin_level_kode` int(3) NOT NULL,
  `admin_level_nama` varchar(30) NOT NULL,
  `admin_level_status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_level`
--

INSERT INTO `admin_level` (`admin_level_kode`, `admin_level_nama`, `admin_level_status`) VALUES
(1, 'Administrator', 'A'),
(2, 'Operator', 'A'),
(5, 'Member', 'A'),
(6, 'programmers', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `agenda_id` int(11) NOT NULL,
  `agenda_tema` varchar(100) NOT NULL,
  `agenda_deskripsi` text NOT NULL,
  `agenda_mulai` date NOT NULL,
  `agenda_selesai` date NOT NULL,
  `agenda_tempat` varchar(100) NOT NULL,
  `agenda_jam` varchar(50) NOT NULL,
  `agenda_gambar` varchar(100) DEFAULT NULL,
  `agenda_posting` datetime NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int(3) NOT NULL,
  `album_judul` varchar(100) NOT NULL,
  `album_deskripsi` text NOT NULL,
  `album_gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `album_judul`, `album_deskripsi`, `album_gambar`) VALUES
(29, 'Album 1 Politeknik Pos Indonesia', '<p>\r\n	Album 1 Politeknik Pos Indonesia</p>\r\n', '1441113457-POLPOS.png'),
(30, 'Album 2 Politeknik Pos Indonesia', '<p>\r\n	Album 2 Politeknik Pos Indonesia</p>\r\n', '1441113487-slide1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `album_galeri`
--

CREATE TABLE `album_galeri` (
  `galeri_id` int(5) NOT NULL,
  `galeri_judul` varchar(100) NOT NULL,
  `galeri_deskripsi` varchar(250) NOT NULL,
  `galeri_gambar` varchar(100) NOT NULL,
  `galeri_waktu` datetime NOT NULL,
  `album_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `album_galeri`
--

INSERT INTO `album_galeri` (`galeri_id`, `galeri_judul`, `galeri_deskripsi`, `galeri_gambar`, `galeri_waktu`, `album_id`) VALUES
(45, 'Contoh Gallery2', '<p>\r\n	Contoh Gallery2</p>\r\n', '1440333261-slide2.JPG', '2015-08-23 19:34:21', 29),
(46, 'Contoh Gallery4', '<p>\r\n	Contoh Gallery4</p>\r\n', '1440413846-logo poltekpos.png', '2015-08-24 17:56:47', 29),
(47, 'Contoh Gallery1', '<p>\r\n	Contoh Gallery1</p>\r\n', '1440413903-pol1.PNG', '2015-08-24 17:58:23', 29),
(48, 'Contoh Gallery3', '<p>\r\n	Contoh Albums</p>\r\n', '1440413943-pol2.PNG', '2015-08-24 17:59:03', 29),
(49, 'Contoh Gallery5', '<p>\r\n	Contoh Gallery5</p>\r\n', '1440413987-pol3.PNG', '2015-08-24 17:59:47', 29),
(50, 'Contoh Gallery6', '<p>\r\n	Contoh Gallery6</p>\r\n', '1440414020-DESAIN KEMEJA.png', '2015-08-24 18:00:20', 29),
(51, 'Contoh Gallery7', '<p>\r\n	Contoh Gallery7</p>\r\n', '1440414048-slide2.JPG', '2015-08-24 18:00:48', 29),
(52, 'Contoh Gallery8', '<p>\r\n	Contoh Gallery8</p>\r\n', '1440414112-SEMINAR.jpg', '2015-08-24 18:01:52', 29),
(53, 'Album 2 Politeknik Pos Indonesia', '<p>\r\n	Album 2 Politeknik Pos Indonesia</p>\r\n', '1441113588-launching.jpg', '2015-09-01 20:19:48', 30);

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `berita_id` int(5) NOT NULL,
  `berita_judul` varchar(100) NOT NULL,
  `headline` enum('N','Y') DEFAULT 'N',
  `berita_deskripsi` text NOT NULL,
  `berita_waktu` datetime NOT NULL,
  `berita_gambar` varchar(100) NOT NULL,
  `berita_hits` int(3) DEFAULT NULL,
  `berita_tags` varchar(100) NOT NULL,
  `kategori_id` int(3) NOT NULL,
  `admin_nama` varchar(50) NOT NULL,
  `seen` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`berita_id`, `berita_judul`, `headline`, `berita_deskripsi`, `berita_waktu`, `berita_gambar`, `berita_hits`, `berita_tags`, `kategori_id`, `admin_nama`, `seen`) VALUES
(1, 'Tips Agar Ponsel Tidak Meledak', NULL, 'Baterai adalah bagian yang paling berbahaya dari ponsel, sebab bagian inilah yang paling rentan terbakar dan meledak. Hal ini diungkap oleh pengamat teknologi Alfons Tanujaya.   Selain itu, menurutnya api yang ditimbulkan dari ledakan baterai juga berbahaya karena bersumber dari bahan kimia yang sulit dipadamkan dengan air.   ', '2021-01-17 11:23:28', '0e0f49ac0d9db71f29d8c13490d68bcb.png', NULL, 'Business, Alibaba, Juniper', 6, 'rezaa', 5),
(3, 'Uji Coba dan Sosialisasi Sistem Informasi Tanya dan Konsultasi ( SITAKON )', NULL, 'Uji Coba dan Sosialisasi Sistem Informasi Tanya dan Konsultasi ( SITAKON ) Inspektorat Jenderal Kementerian Perhubungan 26 - 28 Agustus 2020Kegiatan uji coba dan sosialisasi ini dibuka oleh Inspektur I Itjen Kemenhub Sri Lestari Rahayu selaku Koordinator Aplikasi SITAKON dan didampingi Ketua POKJA SITAKON Saipul Yamri#banggasolutions #itdeveloper #itconsultant #itinfrastructure #softwaredevelopment #auditmanagementsystem #riskmanagementsystem #whistleblowingsystem #kemenhub #kementerianperhubungan #itjenkemenhub   ', '2021-01-17 11:26:57', '77850e8717ff611df2c2d19588e89eea.PNG', NULL, 'banggasolution, softwaredevelopment', 3, 'rezaa', 5),
(4, 'PHP Programmer', NULL, 'test', '2021-01-17 11:26:32', 'ffb3066cb129d10cf7d0de62102e2000.jpg', NULL, 'banggasolution', 4, 'rezaa', 4);

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(2525, 1609140828, '103.233.88.224', '1450'),
(2526, 1609140835, '103.233.88.224', '4267'),
(2527, 1609143988, '103.233.88.224', '0345'),
(2528, 1609144027, '103.233.88.224', '6947'),
(2529, 1609144057, '103.233.88.224', '8513');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `download_id` int(5) NOT NULL,
  `download_judul` varchar(50) NOT NULL,
  `download_deskripsi` varchar(250) NOT NULL,
  `download_file` varchar(250) NOT NULL,
  `download_hits` int(5) NOT NULL,
  `download_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fasilitas`
--

CREATE TABLE `fasilitas` (
  `fasilitas_id` int(5) NOT NULL,
  `fasilitas_nama` varchar(255) NOT NULL,
  `fasilitas_deskripsi` text NOT NULL,
  `fasilitas_gambar` varchar(255) NOT NULL,
  `fasilitas_waktu` date NOT NULL,
  `status` int(11) DEFAULT '0',
  `direktori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fasilitas`
--

INSERT INTO `fasilitas` (`fasilitas_id`, `fasilitas_nama`, `fasilitas_deskripsi`, `fasilitas_gambar`, `fasilitas_waktu`, `status`, `direktori`) VALUES
(27, 'rr', 'www', '3151d183e41f736b5e7fe7fcf48c5b17.png', '2021-01-21', NULL, 'll'),
(28, 'test', 'ee', 'f2d319b88b5c40eed418d26ba0c4f4e2.png', '2021-01-21', NULL, 'eeeeeee');

-- --------------------------------------------------------

--
-- Table structure for table `foto`
--

CREATE TABLE `foto` (
  `id` int(4) NOT NULL,
  `nama_foto` varchar(90) NOT NULL,
  `get_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foto`
--

INSERT INTO `foto` (`id`, `nama_foto`, `get_id`) VALUES
(21, '9e696e139bead2629300da02d8d5b79a.PNG', 18),
(22, 'e3a5e0a7eabfbe3c66d3d01ef91aa536.PNG', 19),
(23, '9c412976de08a596923458548e4036a3.PNG', 19),
(24, 'd7c36f98b69a3ca1881680dc377d159c.jpg', 17),
(25, '6b39fac43d4fe86f48bfe5c6679f468f.jpg', 17),
(26, 'd54241cfe04c97b6f6f0a6693e1b45e8.jpg', 21),
(27, 'dca72551df5294e87fec44c255ad39bc.jpg', 26),
(29, 'a4d42cd320b7452ba6cc588925d2c646.jpg', 27),
(30, '7c510a10a77ab4c6add21648b1e2bf12.jpg', 27),
(31, '11727781680cb6131a231094fefe210e.PNG', 28),
(32, 'b1fb47ab8d1f8a611eb3d4a01901fb29.PNG', 28),
(33, '7d2339d085e841bc8ee765a373104cc2.PNG', 28);

-- --------------------------------------------------------

--
-- Table structure for table `galeri_video`
--

CREATE TABLE `galeri_video` (
  `video_id` int(11) NOT NULL,
  `video_judul` varchar(100) NOT NULL,
  `video_deskripsi` varchar(250) NOT NULL,
  `video_link` varchar(200) NOT NULL,
  `video_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri_video`
--

INSERT INTO `galeri_video` (`video_id`, `video_judul`, `video_deskripsi`, `video_link`, `video_waktu`) VALUES
(1, 'test', 'eee', 'eee', '2021-01-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hak_akses`
--

CREATE TABLE `hak_akses` (
  `hak_akses_id` int(3) NOT NULL,
  `deskripsi` varchar(70) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `level_menu` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hak_akses`
--

INSERT INTO `hak_akses` (`hak_akses_id`, `deskripsi`, `menu`, `level_menu`) VALUES
(1, 'bagusss', 'Administrator', 1),
(2, 'bagusSSSS', 'Operator', 2),
(1, 'bagus', 'Website', 2),
(2, 'bagus', 'vission and mission', 1);

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `identitas_id` int(3) NOT NULL,
  `identitas_website` varchar(250) NOT NULL,
  `identitas_deskripsi` text NOT NULL,
  `identitas_keyword` text NOT NULL,
  `identitas_alamat` varchar(250) NOT NULL,
  `identitas_notelp` char(70) NOT NULL,
  `identitas_fb` varchar(100) NOT NULL,
  `identitas_email` varchar(100) NOT NULL,
  `identitas_tw` varchar(100) NOT NULL,
  `identitas_gp` varchar(100) NOT NULL,
  `identitas_yb` varchar(100) NOT NULL,
  `identitas_favicon` varchar(250) NOT NULL,
  `identitas_author` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`identitas_id`, `identitas_website`, `identitas_deskripsi`, `identitas_keyword`, `identitas_alamat`, `identitas_notelp`, `identitas_fb`, `identitas_email`, `identitas_tw`, `identitas_gp`, `identitas_yb`, `identitas_favicon`, `identitas_author`) VALUES
(1, 'BANGGA SOLUTIONS', 'PT. TRI NINDYA UTAMA atau yang lebih dikenal dengan BANGGA SOLUTIONS merupakan perusahaan konsultan teknologi informasi dan jaringan yang berdiri di Jakarta tahun 2012. PT. TRI NINDYA UTAMA menyediakan berbagai macam solusi Teknologi Informasi (TI) dan bisnis proses Termasuk didalamnya pengembangan aplikasi terkini, penerapan infrastruktur TI, pengembangan dan perawatan Website perusahaan penyedia layanan staf TI yang professional di bidangnya.', 'PT. TRI NINDYA UTAMA atau yang lebih dikenal dengan BANGGA SOLUTIONS merupakan perusahaan konsultan teknologi informasi dan jaringan yang berdiri di Jakarta tahun 2012. PT. TRI NINDYA UTAMA menyediakan berbagai macam solusi Teknologi Informasi (TI) dan bisnis proses Termasuk didalamnya pengembangan aplikasi terkini, penerapan infrastruktur TI, pengembangan dan perawatan Website perusahaan penyedia layanan staf TI yang professional di bidangnya.', 'Jl. H. Saaba Raya Kav F1-F2, Meruya Selatan, Kembangan, Jakarta Barat, 11650', 'Phone :(+62)21-5842138', 'https://www.facebook.com/trinindyautama', 'trinindyautama@gmail.com', '-', 'https://plus.google.com/112086280882257752442', '-', 'bs.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `image_lp`
--

CREATE TABLE `image_lp` (
  `id` int(4) NOT NULL,
  `foto1` varchar(90) NOT NULL,
  `foto2` varchar(90) NOT NULL,
  `foto3` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_lp`
--

INSERT INTO `image_lp` (`id`, `foto1`, `foto2`, `foto3`) VALUES
(1, '49a1e2affc78fefb250b43ae41a0fc87.jpg', '20e797bdba889628b4a9b9bd61985797.jpg', '622586a19a75120661cbef514ccce05e.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `join_event`
--

CREATE TABLE `join_event` (
  `join_id` int(3) NOT NULL,
  `join_nama` varchar(30) NOT NULL,
  `agenda_id` int(3) NOT NULL DEFAULT '0',
  `join_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `join_event`
--

INSERT INTO `join_event` (`join_id`, `join_nama`, `agenda_id`, `join_waktu`) VALUES
(13, 'asrul', 12, '2015-09-25 19:33:15'),
(16, 'Jaka', 12, '2015-09-25 21:30:27'),
(17, 'Nava Gia Ginasta', 12, '2015-09-25 22:28:23');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(3) NOT NULL,
  `kategori_judul` varchar(50) NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_judul`, `admin_nama`) VALUES
(3, 'Berita', 'Nava Gia Ginasta'),
(4, 'Jobs', 'Nava Gia Ginasta'),
(5, 'Networking', 'Arief Cholil Ampri'),
(6, 'Teknologi', 'Arief Cholil Ampri'),
(7, 'Games', 'Arief Cholil Ampri'),
(8, 'Digital', 'Arief Cholil Ampri'),
(9, 'Fotografi', 'Arief Cholil Ampri'),
(10, 'Mobile', 'Arief Cholil Ampri'),
(11, 'Android', 'Arief Cholil Ampri');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `komentar_id` int(11) NOT NULL,
  `komentar_nama` varchar(30) NOT NULL,
  `komentar_deskripsi` text NOT NULL,
  `komentar_waktu` datetime NOT NULL,
  `berita_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `language_id` int(11) NOT NULL,
  `language` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`language_id`, `language`) VALUES
(1, 'english');

-- --------------------------------------------------------

--
-- Table structure for table `management`
--

CREATE TABLE `management` (
  `management_id` int(5) NOT NULL,
  `management_nama` varchar(100) NOT NULL,
  `management_jabatan` varchar(100) NOT NULL,
  `management_team` varchar(250) NOT NULL,
  `management_deskripsi` text NOT NULL,
  `management_email` varchar(100) NOT NULL,
  `management_fb` varchar(250) NOT NULL,
  `management_twitter` varchar(250) NOT NULL,
  `management_gp` varchar(250) NOT NULL,
  `management_foto` varchar(250) NOT NULL,
  `management_post` datetime NOT NULL,
  `admin_nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_kode` int(11) NOT NULL,
  `menu_nama` varchar(50) NOT NULL,
  `menu_deskripsi` varchar(50) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_site` enum('A','H') NOT NULL DEFAULT 'A',
  `menu_level` char(1) NOT NULL,
  `menu_subkode` int(11) NOT NULL,
  `menu_urutan` int(2) NOT NULL,
  `menu_status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_kode`, `menu_nama`, `menu_deskripsi`, `menu_url`, `menu_site`, `menu_level`, `menu_subkode`, `menu_urutan`, `menu_status`) VALUES
(1, 'Berandaa', 'Beranda', 'admin', 'A', '1', 0, 1, 'A'),
(3, 'Website', 'Website', 'website', 'A', '1', 0, 2, 'A'),
(7, 'Pengaturan', 'Pengaturan', 'pengaturan', 'A', '1', 0, 7, 'A'),
(9, 'Profil', 'Informasi Profil', '#', 'A', '2', 1, 1, 'H'),
(11, 'Daftar Menu', 'Informasi Daftar Menu', 'pengaturan/menu', 'A', '3', 13, 1, 'A'),
(12, 'Pengaturan Umum', 'Informasi Pengaturan Umum', 'pengaturan/umum', 'A', '2', 7, 1, 'H'),
(13, 'Setting', 'fa fa-cog fa-fw', '#', 'A', '2', 1, 4, 'A'),
(16, 'Daftar Pengguna', 'Daftar Pengguna', 'pengaturan/pengguna/view', 'A', '3', 13, 1, 'A'),
(17, 'Tambah Pengguna', 'Tambah Pengguna', 'pengaturan/pengguna/tambah', 'A', '3', 13, 2, 'H'),
(19, 'Hak Akses Kelompok', 'Hak Akses Kelompok', 'pengaturan/hak_akses', 'A', '3', 13, 4, 'A'),
(79, 'Menu Utama', 'fa-list', '#', 'A', '2', 1, 3, 'A'),
(80, 'MODUL WEB', 'Informasi Website', '#', 'A', '2', 3, 3, 'H'),
(84, 'Agenda / Events', 'Informasi Agenda / Events', 'website/agenda', 'A', '3', 79, 5, 'A'),
(93, 'Public', 'Menu Public', '#', 'A', '1', 0, 0, 'A'),
(94, 'BERANDA', 'Beranda', 'home', 'A', '2', 93, 1, 'H'),
(97, 'Kategori Berita', 'Informasi Kategori Berita', 'website/kategori', 'A', '3', 79, 1, 'H'),
(98, 'Berita', 'Informasi Berita', 'website/berita', 'A', '3', 79, 2, 'A'),
(99, 'Tags', 'Informasi Tags', 'website/tags', 'A', '3', 79, 3, 'A'),
(100, 'Komentar', 'Informasi Komentar', 'website/komentar', 'A', '3', 79, 4, 'A'),
(101, 'Download File', 'Informasi Download File', 'website/download_file', 'A', '3', 79, 6, 'A'),
(104, 'Galeri Video', 'Informasi Galeri Video', 'website/galeri_video', 'A', '3', 79, 9, 'A'),
(105, 'Modul Website', 'fa-laptop', '#', 'A', '2', 1, 2, 'A'),
(106, 'Identitas Website', 'Informasi Identitas Website', 'website/identitas/edit/1', 'A', '3', 105, 1, 'A'),
(157, 'Partners / Mitra Kerja', 'Informasi Partner / Mitra Kerja', 'website/mitra_kerja', 'A', '3', 79, 10, 'A'),
(158, 'Kelompok Pengguna', 'Kelompok Pengguna', 'pengaturan/kelompok_pengguna', 'A', '3', 13, 3, 'A'),
(160, 'Home', ' fa-home', 'admin', 'A', '2', 1, 1, 'A'),
(161, 'Dashboard', 'Dashboard', 'admin', 'A', '3', 160, 1, 'A'),
(162, 'Management Team', 'Informasi Management Team', 'website/management', 'A', '3', 105, 2, 'A'),
(163, 'Home', '#', '#tf-home', 'A', '2', 93, 1, 'A'),
(164, 'Service', '#', '#tf-services', 'A', '2', 93, 2, 'A'),
(165, 'Companies', '#', '#tf-companies', 'A', '2', 93, 3, 'A'),
(166, 'Galeri Foto', '#', '#gl-foto', 'A', '2', 93, 4, 'A'),
(167, 'About Us', '#', '#tf-about', 'A', '2', 93, 5, 'A'),
(168, 'Updates', '#', '#tf-updates', 'A', '2', 93, 6, 'A'),
(169, 'Contact', '#', '#tf-contact', 'A', '2', 93, 7, 'A'),
(170, 'News', '#', 'news', 'A', '3', 168, 1, 'A'),
(171, 'Events', '#', 'events', 'A', '3', 168, 2, 'A'),
(172, 'Jobs', '#', 'jobs', 'A', '3', 168, 3, 'A'),
(173, 'Fasilitas', 'Informasi Fasilitas', 'website/fasilitas', '', '3', 105, 3, 'A'),
(174, 'PRODUCT', 'Product', 'products', 'A', '1', 0, 3, 'A'),
(175, 'vission and mission', '#', 'visi', 'A', '1', 0, 5, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `menu_admin`
--

CREATE TABLE `menu_admin` (
  `menu_admin_kode` int(11) NOT NULL,
  `menu_kode` int(11) NOT NULL,
  `admin_level_kode` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_admin`
--

INSERT INTO `menu_admin` (`menu_admin_kode`, `menu_kode`, `admin_level_kode`) VALUES
(1, 1, 1),
(2, 3, 1),
(3, 7, 1),
(14, 79, 1),
(16, 84, 1),
(24, 80, 1),
(30, 11, 1),
(31, 13, 1),
(32, 16, 1),
(33, 17, 1),
(34, 18, 1),
(35, 19, 1),
(36, 94, 1),
(96, 96, 1),
(102, 9, 5),
(111, 1, 2),
(112, 79, 2),
(210, 95, 1),
(211, 9, 2),
(212, 3, 2),
(213, 84, 2),
(214, 79, 3),
(215, 84, 3),
(217, 98, 1),
(218, 99, 1),
(219, 100, 1),
(220, 101, 1),
(221, 102, 1),
(222, 103, 1),
(223, 104, 1),
(224, 105, 1),
(225, 106, 1),
(226, 107, 1),
(227, 108, 1),
(228, 109, 1),
(229, 110, 1),
(230, 111, 1),
(231, 157, 1),
(232, 158, 1),
(233, 12, 1),
(234, 98, 2),
(235, 100, 2),
(236, 101, 2),
(237, 103, 2),
(249, 105, 2),
(250, 108, 2),
(251, 9, 4),
(252, 79, 4),
(254, 98, 4),
(255, 100, 4),
(256, 84, 4),
(257, 101, 4),
(258, 103, 4),
(259, 104, 4),
(260, 105, 4),
(261, 108, 4),
(262, 1, 4),
(263, 3, 4),
(264, 160, 1),
(265, 161, 1),
(266, 97, 1),
(267, 162, 1),
(268, 173, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mitra_kerja`
--

CREATE TABLE `mitra_kerja` (
  `mitra_id` int(5) NOT NULL,
  `mitra_nama` varchar(100) NOT NULL,
  `mitra_gambar` text NOT NULL,
  `mitra_link` varchar(250) NOT NULL,
  `mitra_waktu` datetime NOT NULL,
  `jenis_produk` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mitra_kerja`
--

INSERT INTO `mitra_kerja` (`mitra_id`, `mitra_nama`, `mitra_gambar`, `mitra_link`, `mitra_waktu`, `jenis_produk`) VALUES
(1, 'Kementrian Pendidikan Dan Kebudayaan', '4e56d1b49ed56ccea4aafd27778df7ca.PNG', 'http://simontila.itjen.kemdikbud.go.id', '2021-01-10 00:00:00', 1),
(2, 'Kementerian Pendayagunaan Aparatur Negara dan Reformasi Birokrasi', 'e1011b428644ab383bed97308cbf5ee2.PNG', 'http://e-mas.hekya.id', '2021-01-10 00:00:00', 1),
(3, 'Kementrian Perhubungan', '357eb3c2aa3e2a815ffc957353e19bd7.PNG', 'https://siau-itjen.dephub.go.id', '2021-01-10 00:00:00', 1),
(4, 'Kementrian Pertahanan', 'f8c8fa60635d512f74a62763b7968691.PNG', 'https://siau-itjen.dephub.go.id', '2021-01-10 00:00:00', 1),
(5, 'Hutama Karya', '591ad1551de3aa12117c74192e7cf230.PNG', 'https://rms.hutamakarya.com/auth', '2021-01-10 00:00:00', 2),
(6, 'Adhi Karya', '45febdfde2884b4b7918841df3349fb3.png', 'https://banggasolution.com/erisk/adhikarya/demo', '2021-01-11 00:00:00', 2),
(7, 'Percetakan Uang Republik Indonesia', '61881c1a076cc63370fc042fe404f15e.PNG', 'https://banggasolution.com/erisk/peruri/', '2021-01-11 00:00:00', 2),
(8, 'PT Samuel Aset Manajemen (SAM)', '3694e2622a4342985f854eaf5a20db0f.PNG', 'https://banggasolution.com/erisk/samuel/auth', '2021-01-11 00:00:00', 2),
(9, 'Inalum (Indonesia Asahan Aluminium)', 'a97040dbf20e9da6e3a361cd1eb49ce0.PNG', 'https://banggasolution.com/erisk/inalum/demo', '2021-01-11 00:00:00', 2),
(10, 'Hutama Karya', '006ae6988857c3577b62ca5816d32c1b.PNG', 'https://kms.hutamakarya.com/wbs/', '2021-01-11 00:00:00', 3),
(11, 'Adhi Karya', '3b92b9f008508eaefd528944506b2555.PNG', 'http://wbs.adhi.co.id', '2021-01-11 00:00:00', 3),
(12, 'Kementrian Pertahanan', 'bd0711bd536b00d0b5b27fb48c607956.PNG', 'https://wbs.itjen.kemhan.go.id', '2021-01-11 00:00:00', 3),
(13, 'Kementrian Pendidikan Dan Kebudayaan', '463a505176eaa6975252bba72a4b50f9.PNG', 'https://wbs.kemdikbud.go.id', '2021-01-11 00:00:00', 3),
(14, 'Badan Meteorologi Klimatologi Geofisika', '6109bf24d1b67f4e19cf474d120c96ed.PNG', 'http://epengawasan.bmkg.go.id/wbs/', '2021-01-11 00:00:00', 3),
(15, 'Kementrian Perhubungan', '77c4aa4651334c81c598cb0f0794b450.PNG', 'https://simadu.dephub.go.id', '2021-01-11 00:00:00', 3),
(16, 'PT. Donggi-Senoro LNG', '9dc6769a8b0776ed2fa2ee1a96dbf626.PNG', 'https://wbs.donggisenorolng.co.id', '2021-01-11 00:00:00', 3),
(17, 'Hutama Karya', '64c3fec233b58c9d536aac7c7a754bba.PNG', 'https://kms.hutamakarya.com', '2021-01-11 00:00:00', 4),
(18, 'Kementrian Kesehatan', 'beaf001956cfe1537dae97038ff32a4e.PNG', 'https://banggasolution.com/#', '2021-01-11 00:00:00', 4),
(19, 'Hutama Karyaq', '23e15e4f870acf7a471732fa2ee169d4.PNG', 'https://eoffice.hutamakarya.com/login', '2021-01-17 00:00:00', 5),
(20, 'Kementrian Pertahanan', 'f0deb803eb9b11824d3e781206c63a42.PNG', 'https://banggasolution.com/earsip/login', '2021-01-11 00:00:00', 5),
(21, 'Kementrian Pendidikan Dan Kebudayaan', '53a925f512a32277f822201b2a98e82d.PNG', 'https://banggasolution.com/#', '2021-01-18 00:00:00', 5),
(22, 'Hutama Karya', '301c5f1aae12aba40c82650ab18a5a30.PNG', 'https://kpku.hutamakarya.com', '2021-01-11 00:00:00', 6),
(23, 'Wijaya Karya (WIKA)', 'c7a97488415355da06ffe3dcfa2448dc.PNG', 'https://wzone.wika.co.id/index.php/login?next=/', '2021-01-11 00:00:00', 6),
(24, 'Waskita Karya', 'fe11e47e68707f3d9cac91f81d7e61d4.PNG', 'https://banggasolution.com/#', '2021-01-11 00:00:00', 6);

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` int(10) NOT NULL,
  `dari` varchar(100) NOT NULL,
  `nama` varchar(90) NOT NULL,
  `no_tlp` varchar(17) NOT NULL,
  `aplikasi` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  `datetime` datetime NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id`, `dari`, `nama`, `no_tlp`, `aplikasi`, `pesan`, `datetime`, `status`) VALUES
(1, 'testpercobaan111@gmail.com', 'danang', '', 'BANGGA e-TILA BANGGA e-Risk', 'Nama danangPermintaan request demo aplikasi BANGGA e-TILA BANGGA e-Risk eee', '2021-01-13 12:14:21', 'Tidak Terkirim'),
(2, 'nangjipang@gmail.com', 'danang', '', 'BANGGA e-TILA BANGGA e-Risk', 'Nama danangPermintaan request demo aplikasi BANGGA e-TILA BANGGA e-Risk ssss', '2021-01-13 12:31:48', 'Terkirim'),
(3, 'nangjipang@gmail.com', 'danang', '333', 'BANGGA e-TILA BANGGA e-Risk', 'Nama danangPermintaan request demo aplikasi BANGGA e-TILA BANGGA e-Risk dddd 333', '2021-01-17 12:14:05', 'Terkirim');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(3) NOT NULL,
  `nama_produk` varchar(90) NOT NULL,
  `deskripsi_indo` text NOT NULL,
  `deskripsi_ing` text NOT NULL,
  `image` varchar(90) NOT NULL,
  `dokumen` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_produk`, `deskripsi_indo`, `deskripsi_ing`, `image`, `dokumen`, `icon`) VALUES
(1, 'BANGGA e-TILA', '  Secara khusus, manfaat yang dapat dirasakan oleh pengguna dengan mengimplementasikan BANGGA e-TILA antara lain adalah :\r\n\r\nAuditor Intern dan Manajemen Audit Intern Sebagai alat bantu pihak pengawasan internal untuk menjalankan aktivitas, koordinasi dan dokumentasi hasil pemeriksaan secara terotomasi dan tersentralisasi dengan tetap mengacu kepada standar yang berlaku. Selain itu BANGGA e-TILA juga berfungsi sebagai media komunikasi antara auditor dan auditee berkaitan dengan tindak lanjut atas setiap rekomendasi hasil pemeriksaan.\r\n\r\nAudite Sebagai alat bantu untuk melaporkan aktivitas dan status tindak lanjut atas setiap rekomendasi hasil pemeriksaan, sekaligus sebagai alat bantu untuk memantau efektivitas pengendalian intern pada Auditee yang terkait.\r\n\r\nManajemen Pusat Sebagai media utama untuk melakukan koordinasi dengan fungsi pemeriksaan intern, terkait dengan pemantauan atas efektivitas dan efisiensi pelaksanaan pemeriksaan intern.\r\n\r\nAuditor Ekstern Dimungkinkan untuk digunakan sebagai alat bantu guna melakukan koordinasi dan komunikasi dengan fungsi pemeriksaan intern, berkaitan dengan lingkup penugasan audit, temuan dan rekomendasi audit, dengan memberikan hak akses sementara untuk melihat hasil pemeriksaan.  ', '   In particular, the benefits that can be felt by users by implementing BANGGA e-TILA include:\r\n\r\nInternal Auditor and Internal Audit Management As a tool for internal control parties to carry out activities, coordination and documentation of audit results in an automated and centralized manner while still referring to applicable standards. In addition, BANGGA e-TILA also functions as a medium of communication between the auditors and the auditee regarding follow-up on any recommendations on the results of the examination.\r\n\r\nAudite As a tool for reporting activities and follow-up status on each recommendation of examination results, as well as a tool for monitoring the effectiveness of internal control in the related Auditee.\r\n\r\nCentral Management As the main media to coordinate with the internal audit function, related to monitoring the effectiveness and efficiency of the internal examination.\r\n\r\nExternal Auditor It is possible to use it as a tool to coordinate and communicate with the internal audit function, relating to the scope of audit assignments, audit findings and recommendations, by providing temporary access rights to view the audit results.  ', '746a6d567181510e8089ee191c1f0c38.png', '4a7ab4c3e9c1af13d8c2d38eeb83bb57.pdf', '6673f8f144cafc1b0233089c2d2b2592.png'),
(2, 'BANGGA e-Risk', '   Manajemen resiko yang dilaksanakan secara efektif dan wajar dapat memberikan manfaat bagi perusahaan/korporasi, yaitu :\r\n\r\n? Membantu pencapaian tujuan perusahaan.\r\n\r\n? Perusahaan memiliki ukuran kuat sebagai pijakan dalam mengambil setiap keputusan sehingga para manajer dan stakeholder menjadi lebih berhati hati prudent dan selalu menempatkan ukuran ukuran dalam berbagai keputusan.\r\n\r\n? Menghindari biaya biaya yang mengejutkan, karena perusahaan mengidentifikasi dan mengelola risiko yang tidak diperlukan, termasuk menghindari biaya dan waktu yang dihabiskan dalam suatu masalah\r\n\r\n? Meningkatkan akuntabilitas dan corporate governance.\r\n\r\n? Mengubah pandangan terhadap risiko menjadi lebih terbuka, ada toleransi terhadap mistakes tapi tidak terhadap hiding errors Perubahan pandangan ini memungkinkan perusahaan belajar dari kesalahan masa lalunya untuk terus memperbaiki kinerjanya  ', '   Risk management that is carried out effectively and fairly can provide benefits for the company / corporation, namely:\r\n\r\n? Help achieve company goals.\r\n\r\n? The company has a strong measure as a basis for making every decision so that managers and stakeholders become more prudent and always place the measure in various decisions.\r\n\r\n? Avoid shocking costs, as companies identify and manage unnecessary risks, including avoiding costs and time spent on problems\r\n\r\n? Improve accountability and corporate governance.\r\n\r\n? Changing the view of risk to be more open, there is tolerance for mistakes but not against hiding errors This change in perspective allows a company to learn from its past mistakes to continuously improve its performance\r\n  ', 'ff41cfaf7b36fbdd687ea0561a7a34c3.png', '3ac4ebf387f88807af15eba25ac63d02.pdf', '553652b7296354fe81183dc4f74560ac.png'),
(3, 'BANGGA e-WBS', '   System yang cepat, efektif dan efisien bagi masyarakat untuk mengadukan penyimpangan dan indikasi TIPIKOR yang terjadi di Organisasi secara on-line 24 jam.\r\n\r\nSystem yang menjamin kerahasiaan identitas pelapor.\r\n\r\nMemudahkan Aparat Pengawasan Internal Pemerintah (APIP) atau Unit Internal Audit dalam memantau aduan masyarakat secara real time.\r\n\r\nMemudahkan APIP atau Unit Internal Audit dalam melakukan penelusuran “Status Tindak Lanjut” atas setiap aduan masyarakat.\r\n\r\nMenyajikan Laporan atau Rekapitulasi untuk pimpinan secara cepat dan akurat dalam bentuk tabel dan grafik yang menggambarkan jumlah pengaduan masyarakat yang dapat dilihat per Satuan Kerja (satker), per Provinsi, per Tahun atau per Jenis Aduan.  ', '   A fast, effective and efficient system for the public to complain about irregularities and indications of TIPIKOR that occur in the Organization on-line 24 hours.\r\n\r\nA system that ensures the confidentiality of the reporter\'s identity.\r\n\r\nMake it easy for the Government Internal Supervisory Apparatus (APIP) or the Internal Audit Unit to monitor public complaints in real time.\r\n\r\nMake it easy for APIP or the Internal Audit Unit to trace “Follow-up Status” for every public complaint.\r\n\r\nPresenting reports or recapitulations for leaders quickly and accurately in the form of tables and graphs that depict the number of public complaints that can be seen per work unit (satker), per province, per year or per type of complaint.  ', 'ed2f15913cc67a10957bc071464d968f.PNG', '9e58d99a69488efa58a9b97128b7f863.pdf', 'ef4bb9dce457d436379654a6a0d10849.png'),
(4, 'BANGGA KMS', '  KMS adalah system informasi berbasis pengetahuan yang mendukung penciptaan,pengaturan, dan penyebaran dari pengetahuan program kepada pegawai dan pejabat struktural dari sebuah organisasi/perusahaan.\r\n\r\nKnowledge Management berguna untuk pendokumentasian pengetahuan, sehingga dapat menjadi keunggulan kompetitif bagi organisasi/perusahaan.  ', '   KMS is a knowledge-based information system that supports the creation, organization, and dissemination of program knowledge to employees and structural officers of an organization / company.\r\n\r\nKnowledge Management is useful for documenting knowledge, so that it can be a competitive advantage for organizations / companies.  ', 'a027738a27b8d647ea16c284ade1d237.PNG', '1526e7e3ef6c81d3ada3fba09fa7847d.pdf', '3a05dc9e47e1045aa385814b2e7d8c6b.png'),
(5, 'BANGGA e-Officee', '         Melalui semua surat yang dibuat, dikirimkan ataupun diterima, terekam dengan baik dalam jaringan internet/intranet sehingga akan lebih mudah untuk mencari surat yang diinginkan dalam waktu tertentu.\r\n\r\nE-OFFICE juga menghemat penggunaan sumber daya, seperti waktu dan biaya karena semua surat yang ada disimpan dan dibuat secara elektronik.\r\n\r\nMelalui E-OFFICE, instansi publik juga akan menghemat waktu dan biaya dalam penyampaian surat-menyurat antar stakeholders.\r\n\r\nDapat diakses dengan mudah tanpa ada batasan waktu dan tempat dan dapat diakses oleh banyak user dalam satu waktu.        ', '        Through all letters that are made, sent or received, recorded properly on the internet / intranet network so that it will be easier to find the desired letter within a certain time.\r\n\r\nE-OFFICE also saves the use of resources, such as time and costs because all mail is stored and created electronically.\r\n\r\nThrough E-OFFICE, public agencies will also save time and money in delivering correspondence between stakeholders.\r\n\r\nCan be accessed easily without limitation of time and place and can be accessed by many users at one time.        ', '9731262e5c49bcd5f781368a630e8129.png', '7dd4771bfc1d4b5e2021678367915ae7.pdf', '91359dd94fbd1892c53510f906951f89.png'),
(6, 'BANGGA KPKU', '   https://banggasolution.com/kpku/  ', '   https://banggasolution.com/kpku/  ', 'f044f5b36ad9e865ebfc4f477aead697.png', '987f2e7617f5822a471ad6c98146c5be.pdf', '56cf93b36e85c6c835489d3ddb12d70a.png');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('0f30c0056a72e6c120ae90c8b197b747', '103.105.28.165', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609144782, ''),
('1d68cee9cf09a1a8038bd368c4857da2', '103.233.88.224', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36 OPR/7', 1609145689, 'a:5:{s:9:\"user_data\";s:0:\"\";s:10:\"admin_user\";s:5:\"admin\";s:10:\"admin_nama\";s:4:\"Reza\";s:11:\"admin_level\";s:1:\"1\";s:9:\"logged_in\";b:1;}'),
('6debd79e2e9f28f5f9922480a847786a', '103.233.88.224', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 1609140670, ''),
('9ac0f5fe900c215773acd71d9b9720ae', '54.36.149.29', 'Mozilla/5.0 (compatible; AhrefsBot/7.0; +http://ahrefs.com/robot/)', 1609137494, ''),
('fe4fca90205d25af257e1af98d12a002', '114.5.209.32', 'Mozilla/5.0 (Linux; Android 10; M2003J15SC) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.101 Mobile Safari/53', 1609140030, '');

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `slide_id` int(5) NOT NULL,
  `slide_judul` varchar(100) NOT NULL,
  `slide_gambar` varchar(100) NOT NULL,
  `slide_deskripsi` text NOT NULL,
  `slide_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide`
--

INSERT INTO `slide` (`slide_id`, `slide_judul`, `slide_gambar`, `slide_deskripsi`, `slide_waktu`) VALUES
(6, 'Slide2', '1439110065-slide2.JPG', '<p>\r\n	Slide2</p>\r\n', '2015-08-09 14:52:03'),
(7, 'Slide1', '1439109241-slide1.jpg', '<p>\r\n	Slide1</p>\r\n', '2015-08-09 15:34:01'),
(8, 'Slide3', '1441171313-slide3.JPG', '<p>\r\n	Slide3</p>\r\n', '2015-09-02 12:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `statis`
--

CREATE TABLE `statis` (
  `statis_id` int(5) NOT NULL,
  `statis_judul` varchar(100) NOT NULL,
  `statis_deskripsi` text NOT NULL,
  `statis_gambar` varchar(100) NOT NULL,
  `statis_status` enum('N','Y') NOT NULL,
  `statis_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(5) NOT NULL,
  `tag_judul` varchar(50) NOT NULL,
  `tag_seo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`tag_id`, `tag_judul`, `tag_seo`) VALUES
(1, 'Teknologii', 'teknologi'),
(2, 'Bencana Alam', 'bencana-alam'),
(3, 'Business', 'business'),
(4, 'Alibaba', 'alibaba'),
(5, 'Gadged', 'gadged'),
(6, 'Networking', 'networking'),
(7, 'Juniper', 'juniper'),
(8, 'Internet', 'internet'),
(9, '5G', '5g'),
(10, 'Digital', 'digital'),
(11, 'Googlee', 'google'),
(12, 'Fotografi', 'fotografi'),
(13, 'Mobile', 'mobile'),
(14, 'Android', 'android'),
(15, 'banggasolution', 'banggasolution'),
(16, 'riskmanagementsystem', 'riskmanagementsystem'),
(17, 'auditmanagementsystem', 'auditmanagementsystem'),
(18, 'itdeveloper', 'itdeveloper'),
(19, 'itconsultant', 'itconsultant'),
(20, 'itinfrastructure', 'itinfrastructure'),
(21, 'softwaredevelopment', 'softwaredevelopment');

-- --------------------------------------------------------

--
-- Table structure for table `testimoni`
--

CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL,
  `dari` text NOT NULL,
  `aplikasi` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimoni`
--

INSERT INTO `testimoni` (`id`, `dari`, `aplikasi`, `deskripsi`, `image`) VALUES
(302, 'test', 'BANGGA KMS , BANGGA e-Office', 'test', '0187f768766849605ab56bc45dcb1a7b.png'),
(303, 'danang', 'BANGGA e-TILA , BANGGA KPKU', 'sss', '30f3f472cbc88271ce31e2dd9d741214.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `testimonial_id` int(5) NOT NULL,
  `testimonial_nama` varchar(100) NOT NULL,
  `testimonial_sumber` varchar(100) NOT NULL,
  `testimonial_kerja` varchar(200) NOT NULL,
  `testimonial_jabatan` varchar(200) NOT NULL,
  `testimonial_deskripsi` text NOT NULL,
  `testimonial_gambar` varchar(100) NOT NULL,
  `testimonial_waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`testimonial_id`, `testimonial_nama`, `testimonial_sumber`, `testimonial_kerja`, `testimonial_jabatan`, `testimonial_deskripsi`, `testimonial_gambar`, `testimonial_waktu`) VALUES
(3, 'Ali Abdul Wahid', 'Masih Kuliah', 'CV.Nava Teknologi', 'Programmer', 'Poltekpos Indonesia memiliki kampus nyaman untuk belajar, fasilitas lengkap dan dosen-dosen yg ahli di bidangnya. Terima kasih, selesai wisuda saya langsung mendapat pekerjaan.', '1440387703-11206097_907674959270914_6026589404761839948_n.jpg', '2015-08-24 10:41:43'),
(4, 'Mochammad Uki', 'Masih Kuliah', 'CV.Nava Teknologi', 'Web Design', 'Saya merasa senang bisa menjadi Mahasiswa Politeknik Pos Indonesia. Mendapat fasilitas pembelajaran yang baik. Tidak hanya Menerima Pengetahuan Secara Teori, bahkan menerima pengetahuan dari pengalaman-pengalaman para dosen. Sehingga kita siap menghadapi dunia kerja.\r\n', '1440388399-1001594_419773781456187_112239529_n.jpg', '2015-08-24 10:53:19'),
(5, 'Nava Gia Ginasta', 'Masih Kuliah', 'KOMINFO', 'Programmer Analyst & Project Leader', 'Politeknik Pos Indonesia memiliki kampus yang sangat memadai dibandingkan dengan politeknik yang lain. Proses belajar mengajar jadi lebih nyaman. Materi kuliahnya pun bagus khususnya untuk jurusan Teknik Informatika. Dari tahun ke tahun ada perubahan kurikulum yang mengikuti perkembangan IT. Dengan sistem seperti ini, mahasiswa tidak akan ketinggalan informasi tentang perkembangan IT, dan menyiapkan mahasiswanya untuk siap bersaing di dunia kerja.\r\n', '1440388490-NAVA_FOTO_500MB.jpg', '2015-08-24 10:54:50'),
(6, 'Septian Cahya Diningrat', 'Masih Kuliah', 'CV.Sumedang FC', 'Direktur', 'Program Studi Teknik Informatika Politeknik Pos Indonesia telah memberikan banyak pengetahuan kepada saya baik dalam teori maupun praktek. Lulusan Poltek Pos dituntut untuk mampu bersaing dalam dunia kerja sehingga tidak tetinggal dari lulusan sarjana universitas lain meskipun lulusan diploma. Dosen Poltekpos juga sangat berkualitas dan profesional baik dosen dari Poltek Pos sendiri Maupun Dosen Luar (Tamu) dari Kalangan praktisi atau Universitas lain. Selama menempuh pendidikan saya diberi kesempatan untuk langsung terjun ke dunia kerja, baik berupa proyek maupun kerja praktek. Selain itu saya juga mendapat banyak manfaat dari kerja sama pihak Poltek Pos dengan berbagai vendor ternama seperti Microsoft, Oracle, dan lain-lain, baik berupa pelatihan, seminar, kunjungan industri maupun yang lain. Suatu hal yang cukup penting dan sangat menguntungkan bagi saya yang tidak saya dapatkan dari tempat kuliah saya sebelum masuk ke Poltek Pos. [hide]', '1441027340-septian.jpg', '2015-08-31 20:22:20'),
(7, 'Anggi Sholihatus Sadiah', 'Masih Kuliah', 'Pemda Kab.Subang', 'Sekretaris Daerah', 'seneng banget dah bisa kuliah di politeknik pos indonesia jur tehnik informatika, banyak dapet ilmu2 yg bermanfaat di dunia kerja loch...\r\nTempat kuliahnya juga nyaman banget buat belajar...\r\nDosennya juga ga kalah okey ..\r\nGa lupa selalu Bikin kangen kuliah dsana lagi ney..\r\nDan tak terlupakan dah dpt banyak banget pengalaman selama kuliah di poltekpos pokoknya...\r\n', '1441027469-anggi.jpg', '2015-08-31 20:24:29'),
(8, 'Rizki Fadillah D\'kenjie', 'Masih Kuliah', 'Riau Hutan, Tbk', 'Manager', 'Saya adalah salah satu alumni dari Politeknikpos Indonesia dari jurusan Teknik Informatika. Poltekpos adalah kampus yang dibangun sangat kondusif bagi mereka yang ingin pandai dan meraih sukses. Dan yang membuat saya merasa nyaman adalah fasilitas laboratorium yang sangat mendukung, tim pengajar yang professional dan berkualitas di bidangnya, lingkungan yang bersih, dan kekeluargaan yang tercipta antara dosen dan mahasiswanya. Ilmu yang saya dapatkan di Poltekpos mampu mengantar saya untuk siap bersaing serta memulai langkah sukses.', '1441027729-rizki.jpg', '2015-08-31 20:28:49');

-- --------------------------------------------------------

--
-- Table structure for table `visitor`
--

CREATE TABLE `visitor` (
  `ip_address` varchar(255) NOT NULL,
  `dari` varchar(90) NOT NULL,
  `os` varchar(50) NOT NULL,
  `browser` varchar(50) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visitor`
--

INSERT INTO `visitor` (`ip_address`, `dari`, `os`, `browser`, `date`) VALUES
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 10:54:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:32:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:32:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:33:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:36:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:36:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:37:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:37:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:38:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:38:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:42:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:44:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:45:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:45:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:46:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:46:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:47:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:47:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:47:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:49:01'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:49:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 11:53:18'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 20:15:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-17 23:21:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:09:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:13:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:15:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:16:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:16:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:17:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:17:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:18:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 00:20:48'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 06:04:50'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 06:16:38'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 06:30:43'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:22:08'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:27:59'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:28:03'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:33:12'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:34:07'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:34:10'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:35:26'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:36:32'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:36:43'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:37:52'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:37:56'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:39:18'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:39:22'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:41:01'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:41:20'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:41:48'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:42:58'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:43:48'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:44:05'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:45:23'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:45:38'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:45:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:45:57'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:46:18'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:47:38'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:48:17'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:49:31'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:49:33'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:50:02'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:50:09'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:51:21'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 07:52:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 13:55:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:02:27'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:04:28'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:10:22'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:10:35'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:13:43'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:14:09'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:14:21'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:14:39'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:14:40'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:14:55'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:16:14'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:17:43'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:18:23'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:18:38'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:18:55'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:19:23'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:19:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:21:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:22:21'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:22:34'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:22:51'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:23:09'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:23:47'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:24:25'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:24:34'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:24:52'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:25:07'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:25:42'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:25:58'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:26:09'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:26:30'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:26:42'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:28:01'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:29:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:29:18'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:29:26'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:29:43'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:30:23'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:30:32'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:31:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:31:15'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:33:05'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:33:33'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:34:07'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:34:48'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:35:01'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:35:24'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:35:50'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:36:05'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:36:19'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:36:31'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:36:51'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:37:32'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:37:48'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:38:07'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:38:26'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:38:52'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:40:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:42:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:43:41'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:46:52'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:47:47'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:48:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:49:50'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 14:50:44'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:02:42'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:07:28'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:08:27'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:12:55'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:20:33'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:21:16'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:32:00'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:34:42'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:36:20'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:36:36'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:36:49'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:37:02'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:37:44'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:40:49'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:43:32'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:44:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:44:38'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:45:59'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:46:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:51:42'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:53:20'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:54:08'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:55:51'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:56:05'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:56:13'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:56:22'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:58:57'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:59:40'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 15:59:52'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 16:00:55'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 16:08:37'),
('::1', 'Bekasi - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 17:00:46'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:43:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:46:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:51:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:52:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:53:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:54:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:54:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:57:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:58:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:58:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:59:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:59:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 21:59:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:00:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:00:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:01:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:03:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:04:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:05:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:05:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:05:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:06:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:08:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:10:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:13:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:14:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:17:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:18:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:19:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:21:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:21:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:22:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:23:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:23:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:24:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:25:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:25:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:26:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:28:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:30:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:31:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:31:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:32:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:34:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:35:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:35:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:36:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:39:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:40:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:40:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:41:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:41:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:41:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:42:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:42:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:43:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:44:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:51:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 22:52:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 23:23:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 23:51:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 23:51:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 23:52:43'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-18 23:56:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:20:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:22:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:24:56'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:26:22'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:27:00'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:29:01'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:30:21'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:30:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:31:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:32:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:33:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:33:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:33:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:34:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:34:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:36:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:37:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:38:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:39:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:39:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:40:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.329', '2021-01-19 00:41:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:35:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:47:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:48:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:48:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:49:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:50:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:51:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:51:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:52:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:52:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:52:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:53:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:53:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:54:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:54:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:56:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 09:56:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:00:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:00:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:00:59'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:03:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:03:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:03:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:04:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:05:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:06:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:08:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:09:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:09:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:09:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:09:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:09:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:09:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:22:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:23:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:23:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:29:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:36:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:36:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:37:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:38:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:40:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:42:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:42:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:42:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:49:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:54:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:54:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:54:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:54:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:54:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:55:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:56:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:56:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:56:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:56:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:57:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:57:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:57:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:58:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:58:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 10:59:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 11:00:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 11:02:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 13:52:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:04:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:05:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:08:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:09:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:10:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:23:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:27:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:28:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:30:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:32:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:42:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:44:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:46:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:47:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:49:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:50:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:52:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:55:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:55:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:55:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:55:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:56:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 14:57:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:00:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:02:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:03:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:04:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:04:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:20:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:22:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:23:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:24:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:25:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:26:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:27:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:27:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:30:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:31:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:32:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:34:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:35:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:36:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:36:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:36:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:37:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:38:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:39:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:40:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:40:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:41:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:42:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:43:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:43:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:49:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:50:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:50:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:51:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:52:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:53:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:53:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:54:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:55:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:56:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:56:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:57:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:58:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:58:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:59:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 15:59:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:01:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:01:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:02:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:03:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:06:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:07:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:07:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:07:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:08:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:08:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:10:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:10:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:12:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:14:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:15:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:15:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:15:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:16:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:17:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:18:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:20:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:21:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:21:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:22:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:23:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:24:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:25:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:26:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:26:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:27:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:29:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:31:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:31:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:32:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:32:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:32:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:34:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:36:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:36:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:37:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:37:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:39:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:39:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:40:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:41:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:42:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:43:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:43:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:45:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:45:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:46:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:47:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:47:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:49:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:51:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:55:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:55:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:55:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:56:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:56:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:56:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:58:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 16:58:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:00:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:02:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:02:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:07:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:08:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:08:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:09:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:09:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:11:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:11:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:12:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:13:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:13:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:14:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:15:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:18:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:25:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:25:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:27:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:27:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:28:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:28:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:29:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:30:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:31:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 17:31:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 23:03:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 23:04:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 23:04:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 23:04:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-19 23:05:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:00:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:04:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:05:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:06:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:07:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:08:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:09:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:09:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:09:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:10:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:11:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:12:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:13:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:13:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:13:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:13:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:14:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:15:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:17:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:17:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:18:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:19:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:19:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:20:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:20:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:21:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:23:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:24:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:26:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:28:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:29:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:30:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:33:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:38:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:38:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:40:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:41:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:41:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 00:47:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 01:02:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 01:02:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 06:53:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 06:57:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 06:59:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:02:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:02:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:03:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:03:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:04:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:04:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:04:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:05:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:05:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:06:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:07:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:08:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:08:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:10:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:10:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:11:25'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:11:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:13:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:14:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:15:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:16:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:20:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:20:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:20:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:22:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:22:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:22:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:23:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:27:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:28:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:29:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:30:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:30:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:31:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:32:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:32:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:36:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:39:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:39:38');
INSERT INTO `visitor` (`ip_address`, `dari`, `os`, `browser`, `date`) VALUES
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:39:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:40:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:40:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:41:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:42:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:43:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:46:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:47:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:47:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:48:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:48:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:48:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:48:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:51:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:53:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:53:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:55:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:56:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:58:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 07:59:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:00:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:03:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:04:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:05:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:05:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:09:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:10:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:11:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:11:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:12:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:13:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:13:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:13:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:14:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:14:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:15:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:16:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:16:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:17:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:17:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:18:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:18:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:19:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:21:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:23:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:24:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:25:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:26:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:26:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:27:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:27:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:28:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:28:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:28:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:29:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:29:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:29:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:30:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:31:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:32:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:32:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:33:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:33:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:33:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:34:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:35:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:36:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:36:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:37:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:37:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:38:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:38:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:39:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:39:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:40:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:42:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:42:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:43:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:44:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:44:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 08:46:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:01:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:02:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:05:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:16:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:20:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:24:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:26:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:41:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:41:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:44:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 09:59:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:04:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:06:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:06:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:10:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:11:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:13:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:19:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:21:34'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:28:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:29:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:29:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:29:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:31:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:31:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:32:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:32:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:32:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:32:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:32:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:33:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:36:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:37:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:39:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:39:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:40:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:42:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:43:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:43:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:45:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:45:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 10:48:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 14:52:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:00:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:02:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:03:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:08:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:09:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:09:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:10:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:12:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:12:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:12:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:13:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:14:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:15:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:22:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:25:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:27:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:27:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:31:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:31:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:38:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:39:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:39:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:40:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:48:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:48:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:49:18'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:49:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:50:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:51:59'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:52:39'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:54:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:54:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:55:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:57:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:57:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:58:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 15:59:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:00:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:02:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:04:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:08:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:09:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:09:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:11:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:13:14'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:13:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:17:00'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:17:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:17:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:18:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:19:41'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:23:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:23:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:23:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:24:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:25:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:27:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:29:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:29:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:33:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:34:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:38:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:39:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:40:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:40:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:41:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:44:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:48:24'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:50:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:53:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:53:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:54:29'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 16:55:44'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:01:24'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:05:20'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:05:47'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:06:08'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:06:37'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:08:09'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:09:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:11:09'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:13:10'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:15:38'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:16:12'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:16:41'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:16:56'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:17:39'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:18:01'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:20:52'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:21:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:30:27'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:30:43'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:33:09'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:41:02'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:42:22'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:44:17'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:44:51'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:45:32'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:45:40'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:47:44'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:48:19'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:48:30'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:52:35'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:53:14'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:54:23'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:54:41'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:55:11'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:55:30'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 17:56:32'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:00:02'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:00:14'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:00:51'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:08:05'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:08:59'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:09:19'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:13:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:14:06'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:14:23'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:15:40'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:17:05'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:17:32'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:22:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:24:28'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:24:52'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:26:07'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:26:15'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:26:32'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:29:39'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:31:25'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:31:48'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 18:39:25'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 19:42:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 20:18:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 20:19:18'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 22:34:55'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 22:35:02'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 22:35:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:40:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:41:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:42:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:44:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:45:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:47:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-20 23:51:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:11:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:12:09'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:12:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:12:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:12:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:15:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:17:39'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:23:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:26:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:26:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:27:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:29:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:29:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:29:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:36:21'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:39:17'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:39:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:41:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:44:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:45:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:47:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:49:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:49:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:51:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:52:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:53:42'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 00:55:14'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 06:45:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:05:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:09:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:11:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:14:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:18:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:27:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:29:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:29:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:45:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 07:46:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:11:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:13:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:15:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:17:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:22:10'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:27:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:28:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:29:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:31:49'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:33:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:37:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:46:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:48:30'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:49:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:49:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:54:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 08:59:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:00:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:03:54'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:05:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:06:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:10:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:11:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:12:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:13:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:14:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:17:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:17:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:18:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:19:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:20:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:22:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:25:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:25:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:34:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:39:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:41:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:41:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:44:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:45:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:46:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:50:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:51:25'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:52:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 09:52:35'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:11:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:12:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:13:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:18:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:22:36'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:25:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 10:27:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 11:18:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:20:55'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 11:21:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:22:57'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:27:40'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:31:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:33:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 11:34:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 11:36:38'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:38:51'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 11:39:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 11:42:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 11:59:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:22:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:22:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:23:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:23:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:23:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:24:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:24:27'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:25:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:25:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:26:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:27:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:27:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:29:08'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:29:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 12:30:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:12:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:14:22'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:14:46'),
('::1', ' - ', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:17:56'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:23:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:23:59'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:26:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:28:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:29:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:29:23'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 14:33:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 14:39:21'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 14:39:31'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 14:43:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 14:45:24'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 14:48:49'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 14:49:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:50:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:51:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:53:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:54:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 14:56:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:04:12'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:07:03'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:11:26'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:14:11'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:14:27'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:21:00'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:22:14'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:26:07'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:33:38'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:34:19'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 15:35:03'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:41:37'),
('::1', 'Jakarta - Indonesia', 'Android', 'Chrome 87.0.4280.141', '2021-01-21 15:43:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:47:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:49:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 15:52:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 16:12:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 16:20:43'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Chrome 87.0.4280.141', '2021-01-21 17:08:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:13:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:15:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:15:44'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:16:52'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:17:46'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:18:53'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:21:02'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:23:51'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:24:32'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:25:20'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 17:27:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 20:26:33'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 20:26:37'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 20:29:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 20:32:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 20:34:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 21:58:04'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 21:58:47'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:02:01'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:03:16'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:04:45'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:05:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:06:28'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:06:48'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:07:29'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:09:06'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:21:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:22:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:27:58'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:30:05'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:30:15'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:34:13'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:35:34'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:37:23'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:37:50'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:38:07'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:39:31'),
('::1', 'Jakarta - Indonesia', 'Windows 10', 'Opera 73.0.3856.344', '2021-01-21 22:43:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_user`),
  ADD KEY `admin_level_kode` (`admin_level_kode`);

--
-- Indexes for table `admin_level`
--
ALTER TABLE `admin_level`
  ADD PRIMARY KEY (`admin_level_kode`);

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`agenda_id`);

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`);

--
-- Indexes for table `album_galeri`
--
ALTER TABLE `album_galeri`
  ADD PRIMARY KEY (`galeri_id`),
  ADD KEY `album_id` (`album_id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`berita_id`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `fasilitas`
--
ALTER TABLE `fasilitas`
  ADD PRIMARY KEY (`fasilitas_id`);

--
-- Indexes for table `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri_video`
--
ALTER TABLE `galeri_video`
  ADD PRIMARY KEY (`video_id`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`identitas_id`);

--
-- Indexes for table `image_lp`
--
ALTER TABLE `image_lp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `join_event`
--
ALTER TABLE `join_event`
  ADD PRIMARY KEY (`join_id`,`agenda_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`komentar_id`),
  ADD KEY `berita_id` (`berita_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `management`
--
ALTER TABLE `management`
  ADD PRIMARY KEY (`management_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_kode`);

--
-- Indexes for table `menu_admin`
--
ALTER TABLE `menu_admin`
  ADD PRIMARY KEY (`menu_admin_kode`),
  ADD KEY `menu_kode` (`menu_kode`),
  ADD KEY `admin_level_kode` (`admin_level_kode`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mitra_kerja`
--
ALTER TABLE `mitra_kerja`
  ADD PRIMARY KEY (`mitra_id`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `statis`
--
ALTER TABLE `statis`
  ADD PRIMARY KEY (`statis_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `testimoni`
--
ALTER TABLE `testimoni`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_level`
--
ALTER TABLE `admin_level`
  MODIFY `admin_level_kode` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `agenda_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `album_galeri`
--
ALTER TABLE `album_galeri`
  MODIFY `galeri_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `berita_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2530;

--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `download_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fasilitas`
--
ALTER TABLE `fasilitas`
  MODIFY `fasilitas_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `foto`
--
ALTER TABLE `foto`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `galeri_video`
--
ALTER TABLE `galeri_video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `identitas`
--
ALTER TABLE `identitas`
  MODIFY `identitas_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `image_lp`
--
ALTER TABLE `image_lp`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `join_event`
--
ALTER TABLE `join_event`
  MODIFY `join_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `komentar_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `management`
--
ALTER TABLE `management`
  MODIFY `management_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `menu_admin`
--
ALTER TABLE `menu_admin`
  MODIFY `menu_admin_kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mitra_kerja`
--
ALTER TABLE `mitra_kerja`
  MODIFY `mitra_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `slide_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `statis`
--
ALTER TABLE `statis`
  MODIFY `statis_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `testimoni`
--
ALTER TABLE `testimoni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=304;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `testimonial_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
