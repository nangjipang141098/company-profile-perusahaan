<?php

class Daftar_menu extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		 
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Setting/Daftar_menu/M_Daftar_menu', 'Menu');
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Daftar_menu/Daftar_menu',
					  'cek_query_edit' => $this->Menu->show_where($get),
					  'cek_query_admin_level' => $this->Menu->level_code()
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Daftar_menu/Daftar_menu'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

		$menu_kode			= $this->input->post('menu_kode');
		$menu_nama			= $this->input->post('menu_nama');
		$menu_deskripsi		= $this->input->post('menu_deskripsi');
		$menu_url			= $this->input->post('menu_url');
		$menu_site			= $this->input->post('menu_site');
		$menu_level			= $this->input->post('menu_level');
		$menu_subkode		= $this->input->post('menu_subkode');
		$menu_urutan		= $this->input->post('menu_urutan');
		$menu_status		= $this->input->post('menu_status');
		

				$data_user1 = array(			
					'menu_kode'			=> $menu_kode,
					'menu_nama'			=> $menu_nama,
					'menu_deskripsi'	=> $menu_deskripsi,
					'menu_url'			=> $menu_url,
					'menu_site'			=> $menu_site,
					'menu_level'		=> $menu_level,
					'menu_subkode'		=> $menu_subkode,
					'menu_urutan'		=> $menu_urutan,
					'menu_status'		=> $menu_status
				);
				$where = array(
					'menu_kode' => $menu_kode
				);	

				$this->Menu->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Setting/Daftar_menu/Daftar_menu');	
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Menu->delete($id);
		redirect('Admin/Setting/Daftar_menu/Daftar_menu');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Setting/Daftar_menu/Daftar_menu'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);

		$menu_kode			= $this->input->post('menu_kode');
		$menu_nama			= $this->input->post('menu_nama');
		$menu_deskripsi		= $this->input->post('menu_deskripsi');
		$menu_url			= $this->input->post('menu_url');
		$menu_site			= $this->input->post('menu_site');
		$menu_level			= $this->input->post('menu_level');
		$menu_subkode		= $this->input->post('menu_subkode');
		$menu_urutan		= $this->input->post('menu_urutan');
		$menu_status		= $this->input->post('menu_status');
		

				$data_user1 = array(			
					'menu_kode'			=> $menu_kode,
					'menu_nama'			=> $menu_nama,
					'menu_deskripsi'	=> $menu_deskripsi,
					'menu_url'			=> $menu_url,
					'menu_site'			=> $menu_site,
					'menu_level'		=> $menu_level,
					'menu_subkode'		=> $menu_subkode,
					'menu_urutan'		=> $menu_urutan,
					'menu_status'		=> $menu_status
				);	
					
				$this->Menu->create($data_user1);
				echo "<script>alert('DATA BERHASIL DITAMBAH')</script>";
				redirect('Admin/Setting/Daftar_menu/Daftar_menu');	
	}
	

}