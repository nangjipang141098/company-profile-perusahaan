<?php

class Image_LP extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		// $this->load->Model('Admin/Modul/Identitas/M_Identitas', 'Identitas');
		$this->load->Model('Admin/Setting/Image_LP/M_Image_LP', 'Image_LP');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
	 	
	}
	
	private function set_upload_options(){   
		//upload an image options
		$config['upload_path'] = './image/fasilitas/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;

		return $config;
	}


	
	public function index()
	{
		$data = array('title' => 'PT. TRI NINDYA UTAMA',
					  'content' => 'Admin/Setting/Image_LP/image_LP'
                     );
                     
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}


	public function ChangeImage(){
		// $data = array('title' => 'Admin TNU',
		// 			  'content' => 'Admin/Setting/Image_LP/image_LP'
		// );
		// $this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
        
        
		$config['upload_path'] = './image/landing_page/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
 
        $this->upload->initialize($config);
         // upload gambar 1
         $this->upload->do_upload('foto1');
         $result1 = $this->upload->data();
         $dokumen1 = $result1['file_name'];
         // upload gambar 2
         $this->upload->do_upload('foto2');
         $result2 = $this->upload->data();
         $dokumen2 = $result2['file_name'];
         // upload gambar 3
         $this->upload->do_upload('foto3');
         $result3 = $this->upload->data();
         $dokumen3 = $result3['file_name'];
        
        echo $dokumen1. " ". $dokumen2. " ". $dokumen3." ". $this->input->get('id');
			
				$data_user1 = array(
					'foto1' => $dokumen1,
                    'foto2' => $dokumen2,
                    'foto3' => $dokumen3
				
				);
				$where = array(
					'id' => $this->input->get('id')
                );

			    $this->Image_LP->edit($where, $data_user1);
		redirect("Admin/Setting/Image_LP/Image_LP");
	}

	public function edit_identity(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Identitas/Identitass'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	

				$admin_user			= $this->input->post('admin_user');
				$admin_nama			= $this->input->post('admin_nama');
				$admin_email		= $this->input->post('admin_email');
				$admin_telepon		= $this->input->post('admin_telepon');
				$admin_level_kode	= $this->input->post('admin_level_kode');
				$admin_status		= $this->input->post('admin_status');
			
				$data_user1 = array(
					'admin_user'		=> $admin_user,
					'admin_nama'		=> $admin_nama,
					'admin_email'		=> $admin_email,
					'admin_telepon'		=> $admin_telepon,
					'admin_level_kode'	=> $admin_level_kode,
					'admin_status'		=> $admin_status
				);
				$where = array(
					'admin_user' => $admin_user
				);
				$this->Management->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Management/Management');
			        
				
	}


}