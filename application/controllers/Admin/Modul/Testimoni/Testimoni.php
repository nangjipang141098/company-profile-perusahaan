<?php

class Testimoni extends CI_Controller{
	
    public function __construct(){
		parent::__construct();
		
		if(($this->session->userdata('admin_status') != 'A') && ($this->session->userdata('admin_user') == null)){
			$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible fade show" role="alert">
			Anda Belum Login
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>');
		redirect('Login/Login/login');
		}
		
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper('url');
		$this->load->Model('Admin/Modul/Testimoni/M_Testimoni', 'Testimoni');
		$this->load->Model('Admin/Modul/Produk/M_Produk', 'Produk');
	}
	
	public function index()
	{
		$get = "";
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Testimoni/Testimoni',
					  'cek_query_edit' => $this->Testimoni->show_where($get)
	   );
	   
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	}

	private function config_upload(){
		$config['upload_path'] = './image/testimoni/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
		return $config;
	}

	public function edit(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Testimoni/Testimoni'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
	    $this->upload->initialize($this->config_upload());
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$id				= $this->input->post('id');
				$dari			= $this->input->post('dari');
				$aplikasi		= implode(' , ', $this->input->post('aplikasi')); 
				$deskripsi		= $this->input->post('deskripsi');	
				$config['image_library']='gd2';
	            $config['source_image']='./image/testimoni/'.$gbr['file_name'];
	            $config['new_image']= './image/testimoni/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					
					'id'			=> $id,
					'dari'			=> $dari,
					'aplikasi'		=> $aplikasi,
					'deskripsi'		=> $deskripsi,
					'image'			=> $foto
				);
				$where = array(
					'id' => $id
				);
				$this->Testimoni->edit($where, $data_user1);
				echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
				redirect('Admin/Modul/Testimoni/Testimoni');
			
			}
		}
		else{
			
			$id				= $this->input->post('id');
			$dari			= $this->input->post('dari');
			$aplikasi		= implode(' , ', $this->input->post('aplikasi'));
			$deskripsi		= $this->input->post('deskripsi');	
				$data_user1 = array(	
					'id'			=> $id,
					'dari'			=> $dari,
					'aplikasi'		=> $aplikasi,
					'deskripsi'		=> $deskripsi
				);
				$where = array(
					'id' => $id
				);
			$this->Testimoni->edit($where, $data_user1);
			echo "<script>alert('DATA BERHASIL DI EDIT')</script>";
			redirect('Admin/Modul/Testimoni/Testimoni');
			
		}

	        
				
	}
	
	public function delete()
	{
		$id = $this->input->get('id');
		$this->Testimoni->delete($id);
		redirect('Admin/Modul/Testimoni/Testimoni');
	}

	public function add(){
		$data = array('title' => 'Admin TNU',
					  'content' => 'Admin/Modul/Testimoni/Testimoni'
		);
		$this->load->view('template_bootstrap_admin/wrapper', $data, FALSE);
	
		$this->upload->initialize($this->config_upload());
	    if(!empty($_FILES['filefoto']['name'])){
			if ($this->upload->do_upload('filefoto')){
				$gbr = $this->upload->data();
				$id				= $this->input->post('id');
				$dari			= $this->input->post('dari');
				$aplikasi		= implode(' , ', $this->input->post('aplikasi'));
				$deskripsi		= $this->input->post('deskripsi');
				$config['image_library']='gd2';
	            $config['source_image']='./image/testimoni/'.$gbr['file_name'];
	            $config['new_image']= './image/testimoni/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$foto 				= $gbr['file_name'];
				$data_user1 = array(
					
					'id'			=> $id,
					'dari'			=> $dari,
					'aplikasi'		=> $aplikasi,
					'deskripsi'		=> $deskripsi,
					'image'			=> $foto
				);
				$where = array(
					'fasilitas_id' => $id
				);
				$this->Testimoni->create($data_user1);
				echo "<script>alert('DATA BERHASIL DI Tambah')</script>";
				redirect('Admin/Modul/Testimoni/Testimoni');
			
			}
		}
		else{
			
			echo "<script>alert('Gambar wajib dimasukan')</script>";
			redirect('Admin/Modul/Testimoni/Testimoni');
			
		}

	        
				
	}


}