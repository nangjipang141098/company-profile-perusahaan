<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->Model('Admin/Modul/Visitor/M_Visitor', 'Visitor');
		$this->load->Model('Admin/Modul/Produk/M_Produk', 'Produk'); 
		$this->load->Model('Admin/Modul/Partner/M_Partner', 'Partner');
		$this->load->Model('Admin/Modul/Fasilitas/M_Fasilitas', 'Fasilitas');
		$this->load->Model('Admin/Modul/News/M_News', 'News');
		$this->load->Model('Admin/Modul/Testimoni/M_Testimoni', 'Testimoni');
		$this->load->Model('Admin/Modul/Identitas/M_Identitas', 'Identitas');
		$this->load->Model('Admin/Modul/Pesan/M_Pesan', 'Pesan');
		
		$this->load->library('user_agent');
		$this->load->library('phpmailer_lib');
		date_default_timezone_set('Asia/Jakarta');
		
	}
	
	private function set_language(){   
		$data['title'] = $this->lang->line('title');
		$data['home'] = $this->lang->line('home');
		$data['About'] = $this->lang->line('About');
		$data['Services'] = $this->lang->line('Services');
		$data['service_devinition'] = $this->lang->line('service_devinition');
		$data['product_desciption'] = $this->lang->line('product_desciption');
		$data['Portfolio'] = $this->lang->line('Portfolio');
		$data['Testimoni'] = $this->lang->line('Testimoni');
		$data['organization_structure'] = $this->lang->line('organization_structure');
		$data['galery_description'] = $this->lang->line('galery_description');
		$data['Testimoni_description'] = $this->lang->line('Testimoni_description');
		$data['Client'] = $this->lang->line('Client');;
		$data['Client_description'] = $this->lang->line('Client_description');;
		$data['Team'] = $this->lang->line('Team');
		$data['Galery'] = $this->lang->line('Galery');
		$data['Drop_down'] = $this->lang->line('Drop_down');
		$data['contact'] = $this->lang->line('contact');
		$data['wellcome'] = $this->lang->line('wellcome');
		$data['wellcome2'] = $this->lang->line('wellcome2');
		$data['video_company'] = $this->lang->line('video_company');
		$data['about_us'] = $this->lang->line('about_us');
		$data['title_about_us'] = $this->lang->line('title_about_us');
		$data['description_about_us'] = $this->lang->line('description_about_us');
		$data['title_vision_mision'] = $this->lang->line('title_vision_mision');
		$data['title_vision'] = $this->lang->line('title_vision');
		$data['description_vision'] = $this->lang->line('description_vision');
		$data['title_mision'] = $this->lang->line('title_mision');
		$data['description_mision'] = $this->lang->line('description_mision');
		$data['Drop_down_corporate'] = $this->lang->line('Drop_down_corporate'); 
		$data['Drop_down_pns'] = $this->lang->line('Drop_down_pns');
		$data['Drop_down_article_media'] = $this->lang->line('Drop_down_article_media'); 

		$data['services'] = $this->lang->line('services');
		$data['app_development'] = $this->lang->line('app_development');
		$data['type_app_develompemnt'] = $this->lang->line('type_app_develompemnt');
		$data['infrastructure_support'] = $this->lang->line('infrastructure_support');
		$data['type_infrastructure_support'] = $this->lang->line('type_infrastructure_support');
		$data['staffing_service_models'] = $this->lang->line('staffing_service_models');
		$data['type_staffing_service_models'] = $this->lang->line('type_staffing_service_models');
		$data['portfolio'] = $this->lang->line('portfolio');
		$data['show_all'] = $this->lang->line('show_all');
		$data['team_description'] = $this->lang->line('team_description');
		$data['latest_update_section'] = $this->lang->line('latest_update_section');
		$data['up_coming_event_section'] = $this->lang->line('up_coming_event_section');

		$data['description_etila'] = $this->lang->line('description_etila');
		$data['description_erisk'] = $this->lang->line('description_erisk');
		$data['description_wbs'] = $this->lang->line('description_wbs');
		$data['description_kms'] = $this->lang->line('description_kms');
		$data['description_eoffice'] = $this->lang->line('description_eoffice');
		$data['description_kppu'] = $this->lang->line('description_kppu');;
		$data['description_upg'] = $this->lang->line('description_upg');

		$data['request_documentation'] = $this->lang->line('request_documentation');
		$data['description_request_documentation'] = $this->lang->line('description_request_documentation');

		$data['Career'] =  $this->lang->line('Career');;

		return $data;
	}

	public function index()
	{
	
		foreach($this->db->get("language")->result_array() as $row);
		
		$this->load->language('dashboard',$row['language']);
		$data = $this->set_language();
		foreach($this->Fasilitas->show()->result_array() as $q1){
			$data['id_g'] = $q1['fasilitas_id'];
		}
			
		$this->load->view('Public/index', $data);
	}

	function set_public_language(){
		$data_l = array('language' => $this->input->get('id')); $where_l = array('language_id' =>  1); $this->db->update("language", $data_l, $where_l);
	
		redirect("welcome");
	}

	function Carrer(){
		$get = $this->input->get('id');
		$this->load->language('dashboard',$get);
		$data = $this->set_language();
		$this->load->view('Public/Career', $data);
	}

	public function struktur()
	{
		$get = $this->input->get('id');
		$this->load->language('dashboard',$get);
		$data = $this->set_language();
		$this->load->view('Public/Structure', $data);
	}

	public function galery_page()
	{
		$id_galeri = $this->input->get('id_galery');
		
		if($id_galeri == null){
			$get = $this->input->get('id');
			$this->load->language('dashboard',$get);
			$data = $this->set_language();
			$jumlah_data = $this->Fasilitas->show()->num_rows();;
			$this->load->library('pagination');
			$config['base_url'] = base_url().'Welcome/galery_page/';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 8;
			$config['next_link'] = '->';
			$config['prev_link'] = '<-';
			
			$from = $this->uri->segment(3);
			$this->pagination->initialize($config);		
			$data['showing'] = $this->Fasilitas->show_page($config['per_page'],$from);
			$this->load->view('Public/galeri_page', $data);
		}
		else{
			
			$get = $this->input->get('id');
			$this->load->language('dashboard',$get);
			$data = $this->set_language();
			$this->load->view('Public/Galeri_preview', $data);
		}

		
	}



	public function News_page()
	{
		
		$get = $this->input->get('id');
		$this->load->language('dashboard',$get);
		$data = $this->set_language();
		$jumlah_data = $this->News->show()->num_rows();;
		$this->load->library('pagination');
		$config['base_url'] = base_url().'Public/news_page/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 10;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		$data['showing'] = $this->News->show_page($config['per_page'],$from);

		$this->load->view('Public/news_page', $data);
	}

	public function artikel()
	{
		$lang = $this->input->get('lang');
		$this->load->language('dashboard',$lang);
		$data = $this->set_language();
		$this->load->view('Public/artikel_detail', $data);

		foreach($this->News->Show_where($this->input->get('id'))->result_array() as $row):
			$data_seen = array(
				'seen' => $row['seen'] + 1,
			);

			$this->db->where('berita_id', $this->input->get('id'));
			$this->db->update('berita', $data_seen);
			
		endforeach;

	}


	public function send()
	{
		
		$to = "testeraku03@gmail.com"; 
		$from = $this->input->post('email'); 
		$nama = $this->input->post('nama');
		$contact = $this->input->post('contact');
		$aplikasi  = implode(' ', $this->input->post('select_request_documentation'));
		$message = $this->input->post('massage'); 
		$pesan = "Nama " . $nama . "Permintaan request demo aplikasi ". $aplikasi ." ".$message." ".$contact;

		
		//  $config['mailtype'] = 'html';
		//  $config['protocol'] = 'smtp';
		//  $config['smtp_host'] = 'smtp.mailtrap.io';
		//  $config['smtp_user'] = 'c079df183357d2';
		//  $config['smtp_pass'] = '9a312284f6ad2a';
		//  $config['smtp_port'] = 2525;
		//  $config['newline'] = "\r\n";
		 $config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl:smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'testeraku03@gmail.com',
			'smtp_pass' => 'Testeraku123',
			'mailtype' => 'text',
			'charset' => "iso-8859-1"
			
		  );

		 $this->load->library('email', $config);
	//	 $this->email->initialize();

		 $this->email->from($from, $nama);
		 $this->email->to($to);
		 $this->email->subject('Request Demo Application');
		 $this->email->message($pesan);

		 if($this->email->send()) {
			  echo 'Email berhasil dikirim';
			  $data = array(
				'dari'		=> $from,
				'nama'		=> $nama,
				'no_tlp'	=> $contact,
				'aplikasi'	=> $aplikasi,
				'pesan'		=> $pesan,
				'datetime'	=> date("Y-m-d h:i:sa"),
				'status'	=> 'Terkirim'
				);
			
				$this->Pesan->create($data);
	
			 // redirect('welcome');
		 }
		 else {
			echo 'Email tidak berhasil dikirim';
			  $data = array(
				'dari'		=> $from,
				'nama'		=> $nama,
				'no_tlp'	=> $contact,
				'aplikasi'	=> $aplikasi,
				'pesan'		=> $pesan,
				'datetime'	=> date("Y-m-d h:i:sa"),
				'status'	=> 'Tidak Terkirim'
				);
			
				$this->Pesan->create($data);
			  echo '<br />';
			  echo $this->email->print_debugger();
			 // redirect('welcome');
		 }

	}

   
}
