<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin || PT. TRI NINDYA UTAMA</title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>template/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?= base_url() ?>template/assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>template/assets/css/sb-admin.css" rel="stylesheet">

  <script src="<?= base_url() ?>https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.16/js/bootstrap-multiselect.min.js" rel="stylesheet" type="text/css" ></script>
  <script src="<?= base_url() ?>https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.16/js/bootstrap-multiselect.js"> </script>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('template/multiple_upload/dropzone.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('template/multiple_upload/basic.min.css') ?>">
  <script type="text/javascript" src="<?php echo base_url('template/multiple_upload/jquery.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('template/multiple_upload/dropzone.min.js') ?>"></script>



  
  
  
</head>
