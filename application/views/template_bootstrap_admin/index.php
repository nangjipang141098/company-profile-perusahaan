<!DOCTYPE html>
<html lang="en">

    <?php require_once('head.php'); ?>

<body id="page-top">

    <?php require_once('navbar.php'); ?>

    <div id="wrapper">
        <?php require_once('sidebar.php'); ?>
            <div id="content-wrapper">
                <div class="container-fluid"> 
                    <?php require_once('content.php'); ?>             
                </div>
                <?php require_once('footer.php'); ?>
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?php require_once('logout_model.php'); ?>

  <?PHP require_once('js.php'); ?>
</body>

</html>
