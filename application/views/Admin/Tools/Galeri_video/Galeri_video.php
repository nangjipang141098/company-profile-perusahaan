<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Video<br>
            
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Judul</th>
                    <th>Deskripsi</th>
                    <th>Link</th>
                    <th>Waktu</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Judul</th>
                    <th>Deskripsi</th>
                    <th>Link</th>
                    <th>Waktu</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Video->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['video_judul'] )?> </td>
                    <td><?php cetak( $row['video_deskripsi'] )?> </td>
                    <td><?php cetak( $row['video_link'] )?> </td>
                    <td><?php cetak( $row['video_waktu'] )?> </td>
                    
        
                    <td>
                      <a href="<?php cetak( base_url('Admin/Tools/Galeri_video/Galeri_video/delete?id=') . $row['video_id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['video_id'] ) ?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['video_id'] ) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Video->show_where($row['video_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url())?>Admin/Tools/Galeri_video/Galeri_video/edit"  method="post" enctype="multipart/form-data">
                                  <div class="form-group">
                                  
                                    <label>Video ID</label>
                                    <input type="text" name="video_id" class="form-control" value="<?php cetak( $edit['video_id'] )?>" placeholder="Id Karyawan" required readonly>

                                    <label>Judul</label>
                                    <input type="text" name="video_judul" class="form-control" value="<?php cetak( $edit['video_judul'] )?>" placeholder="Id Klaim" required>

                                    <label>Deskripsi</label>
                                    <input type="text" name="video_deskripsi" class="form-control" value="<?php cetak( $edit['video_deskripsi'] )?>" placeholder="Jumlah Diajukan" required>
                                    
                                    <label>Link</label>
                                    <input type="text" name="video_link" class="form-control" value="<?php cetak( $edit['video_link'] )?>" placeholder="Jumlah Diajukan" required>
                            
                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak( base_url())?>Admin/Tools/Galeri_video/Galeri_video/add"  method="post" enctype="multipart/form-data">
          <div class="form-group">
                                  
          <div class="form-group">
                                  
               <label>Video ID</label>
               <input type="text" name="id" class="form-control" placeholder="Id Karyawan" required>

              <label>Judul</label>
              <input type="text" name="video_judul" class="form-control"  placeholder="Id Klaim" required>

              <label>Deskripsi</label>
              <input type="text" name="video_deskripsi" class="form-control"  placeholder="Jumlah Diajukan" required>
                                  
              <label>Link</label>
              <input type="text" name="video_link" class="form-control"  placeholder="Jumlah Diajukan" required>
                          
                                  
           </div>

                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>