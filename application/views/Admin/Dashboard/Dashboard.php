
        <!-- Icon Cards-->
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5">Req Demo App</div>
                <div class="mr-5"><?php echo $this->db->get("testimoni")->num_rows() ?> requested </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?= base_url() ?>Admin/Modul/Email/Email">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5">Visitor</div>
                <div class="mr-5"><?php echo $this->db->get("visitor")->num_rows() ?> visitor </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?= base_url() ?>Admin/Modul/Visitor/Visitor">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5">Product</div>
                <div class="mr-5"><?php echo $this->db->get("produk")->num_rows() ?> Product </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?= base_url() ?>Admin/Modul/Produk/Produk">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5">Photo Activities</div>
                <div class="mr-5"><?php echo $this->db->get("fasilitas")->num_rows() ?> Activitie </div>
              </div>
              <a class="card-footer text-white clearfix small z-1" href="<?= base_url() ?>Modul/Fasilitas/Fasilitas">
                <span class="float-left">View Details</span>
                <span class="float-right">
                  <i class="fas fa-angle-right"></i>
                </span>
              </a>
            </div>
          </div>
        </div>

        <!-- Area Chart Example-->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            PT. TRI NINDYA UTAMA</div>
          <div class="card-body">
          <?php foreach($this->db->get("identitas")->result_array() as $row): ?>
               <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Nama Web
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_website']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Meta Description
                        <div class="text-black-50 small"><b><?php cetak( $row['identitas_deskripsi']) ?></b></div>
                    </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-5">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Struktur<br>
                        <img style="width: 1080px;" src="<?php cetak( base_url().'image/identitas/struktur.png' )?>">
                       
                    </div>
                    </div>
                </div>
                <div class="col-lg-12 mb-4">
                    <div class="card bg-warning text-white shadow">
                    <div class="card-body">
                        Address
                        <div class="text-black-50 small"><b><?php cetak($row['identitas_alamat']) ?></b></div>
                    </div>
                    </div>
                </div>
               
          <?php endforeach; ?>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
