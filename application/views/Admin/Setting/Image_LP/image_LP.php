<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Image Landing Page<br>
           
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Image 1</th>
                    <th>Image 2</th>
                    <th>Image 3</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Image 1</th>
                    <th>Image 2</th>
                    <th>Image 3</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                   
              
                    
                    foreach ($this->db->get("image_lp")->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/landing_page/'.$row['foto1'] )?>"></td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/landing_page/'.$row['foto2'] )?>"></td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/landing_page/'.$row['foto3'] )?>"></td>
                    <td>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Edit</button>
                    </td>
                  </tr>
                  
                 
                  
                

                    <!-- Modal -->
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- konten modal-->
                                <div class="modal-content">
                                    <!-- heading modal -->
                                    <div class="modal-header">
                                        <h4 class="modal-title"> Change Image </h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        
                                    </div>
                                    <!-- body modal -->
                                    <div class="modal-body">
                                        <form action="<?php cetak( base_url() )?>Admin/Setting/Image_LP/Image_LP/ChangeImage?id=<?php cetak( $row['id'] )?>"  method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                                        <?php for ($i=1; $i <=3 ; $i++) :?>
                                            <input type="file" name="foto<?php echo $i;?>"  class="form-control">
                                            <?php endfor;?>
                                             <button type="submit" class="btn btn-primary btn-block">Upload</button>
                                        </form><br><br>
                                        <!-- foto -->
                                        <div class="row">
                                     
                                           
                                    </div>
                                    <!-- footer modal -->
                                    <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


 