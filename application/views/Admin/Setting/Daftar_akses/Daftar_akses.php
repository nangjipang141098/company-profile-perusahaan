<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Akses<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Kode Level</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th>Aktion</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Kode Level</th>
                    <th>Nama</th>
                    <th>Status</th>
                    <th>Aktion</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Akses->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                    <td><?php cetak( $row['admin_level_kode'] ) ?> </td>
                    <td><?php cetak( $row['admin_level_nama'] ) ?> </td>
                    <td><?php cetak( $row['admin_level_status'] ) ?> </td>
                    <td>
                      <a href="<?php cetak( base_url('Admin/Setting/Daftar_akses/Daftar_akses/delete?id=') . $row['admin_level_kode'] ) ?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['admin_level_kode'] ) ?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak($row['admin_level_kode']) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->Akses->show_where($row['admin_level_kode']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url())?>Admin/Setting/Daftar_akses/Daftar_akses/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
  
                              <div class="form-group">
                                  
                                    <label>Kode</label>
                                    <input type="text" name="admin_level_kode" class="form-control" value="<?php cetak( $edit['admin_level_kode'] )?>" placeholder="Level Kode" required>

                                    <label>Judul</label>
                                    <input type="text" name="admin_level_nama" class="form-control" value="<?php cetak( $edit['admin_level_nama'] )?>" placeholder="Nama" required>

                                    <label>SEO</label>
                                    <input type="text" name="admin_level_status" class="form-control" value="<?php cetak( $edit['admin_level_status'] )?>" placeholder="Status" required>
                                    
                          

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak( base_url() )?>Admin/Setting/Daftar_akses/Daftar_akses/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

          <div class="form-group">
                                  
            <label>Kode</label>
            <input type="text" name="admin_level_kode" class="form-control"  placeholder="Level Kode" required>

            <label>Nama</label>
            <input type="text" name="admin_level_nama" class="form-control"  placeholder="Nama" required>

            <label>SEO</label>
            <input type="text" name="admin_level_status" class="form-control"  placeholder="Status" required>
                           
                          
                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>