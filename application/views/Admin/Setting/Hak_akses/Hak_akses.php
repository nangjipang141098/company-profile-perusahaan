<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Menu<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Level Menu</th>
                    <th>Deskripsi</th>
                    <th>Menu</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Level Menu</th>
                    <th>Deskripsi</th>
                    <th>Menu</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->HAkses->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                  <td><?php cetak( $row['hak_akses_id'] ) ?> </td>
                    <td><?php cetak( $row['deskripsi'] ) ?> </td>
                    <td><?php cetak( $row['menu'] ) ?> </td>
                    <td><?php cetak( $row['level_menu'] ) ?> </td>
                    <td>
                      <a href="<?php cetak( base_url('Admin/Setting/Hak_akses/Hak_akses/delete?id=') . $row['hak_akses_id'] ) ?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['hak_akses_id'] ) ?>">Edit</a>
                    </td>
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak($row['hak_akses_id']) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                                $cek_query_edit=$this->HAkses->show_where($row['hak_akses_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                            <form action="<?php cetak( base_url())?>Admin/Setting/Hak_akses/Hak_akses/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
  
                              <div class="form-group">
                                  
                                    <label>Hak Akses</label>
                                    <input type="text" name="hak_akses_id" class="form-control" value="<?php cetak( $edit['hak_akses_id'] )?>" placeholder="Level Kode" required>

                                    <label>Deskripsi</label>
                                    <input type="text" name="deskripsi" class="form-control" value="<?php cetak( $edit['deskripsi'] )?>" placeholder="Nama" required>

                                    <label>Level Code</label>
                                    <select name="admin_level_kode" class="form-control">
                                      <?php
                                      $query_edit_level_code=$this->HAkses->level_code();
                                      foreach ($query_edit_level_code->result_array() as $level_kode) {
                                        if ($edit['level_menu']==$level_kode['admin_level_kode']) {
                                            $select="selected";
                                        }else{
                                            $select="";
                                        }
                                        echo "<option $select>".$level_kode['admin_level_nama']."</option>";
                                      }
                                      ?>      
                                    </select>
                                    
                                    <label>Menu</label>
                                    <input type="text" name="level" class="form-control" value="<?php cetak( $edit['level_menu'] )?>" placeholder="Status" required>

                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                   <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  
                                   </form>
                                   <?php } ?>

                                    
                                  </div>
                              
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak( base_url() )?>Admin/Setting/Hak_akses/Hak_akses/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 

          <div class="form-group">
            
          
          <label>Hak Akses</label>
              <input type="text" name="hak_akses_id" class="form-control" placeholder="Level Kode" required>

              <label>Deskripsi</label>
              <input type="text" name="deskripsi" class="form-control" placeholder="Nama" required>
              
              <label>Menu</label>
              <select id="admin_level" class="form-control" name="menu">
                <option>--Pilih Level--</option>
              </select>

              <label> Level</label>
              <select id="nama_menu_pilih"  class="form-control" name="level">
                <option>--Pilih Menu--</option>
              </select>
   
                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>

      <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function(){
          var app = {
            show: function(){
              $.ajax({
                url: "<?php echo base_url() ?>Admin/Setting/Hak_akses/Hak_akses/menu",
                method: "GET",
                success: function(data){
                  $("#admin_level").html(data)
                }
              })
            },
            tampil: function(){
              var menu_nama = $(this).val();
              $.ajax({
                url: "<?php echo base_url() ?>Admin/Setting/Hak_akses/Hak_akses/get_menu",
                method: "GET",
                data: {menu_nama: menu_nama},
                success: function(data){
                  $("#nama_menu_pilih").html(data)
                }
              })
            }
          }
          app.show();
          $(document).on("change", "#admin_level", "#nama_menu_pilih", app.tampil)
        })
      </script>