   <!-- Icon Cards-->
   <div class="row">
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-primary o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-comments"></i>
                </div>
                <?php
                $date = date("Y-m-d");
                $date_tommorow = date("Y-m-d", strtotime('-1 days'));
                $date_7days = date("Y-m-d", strtotime('-1 days'));
                $date_1month = date("Y-m-d", strtotime('-1 month'));
                ?>
                <div class="mr-5">Today</div>
                <div class="mr-5"><?php  echo $this->db->query("SELECT *FROM visitor WHERE date between '$date_tommorow 00:00:81' and '$date 23:59:59'")->num_rows() ?> Visitor </div>
              </div>
          
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-warning o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5">The Last 7 days</div>
                 <div class="mr-5"><?php  echo $this->db->query("SELECT *FROM visitor WHERE date between '$date_7days 00:00:81' and '$date 23:59:59'")->num_rows() ?> Visitor </div>
              </div>
              
              </a>
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-success o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5">The Last 1 Month</div>
                <div class="mr-5"><?php  echo $this->db->query("SELECT *FROM visitor WHERE date between '$date_1month 00:00:81' and '$date 23:59:59'")->num_rows() ?> Visitor </div>
              </div>
              
            </div>
          </div>
          <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card text-white bg-danger o-hidden h-100">
              <div class="card-body">
                <div class="card-body-icon">
                  <i class="fas fa-fw fa-life-ring"></i>
                </div>
                <div class="mr-5">Total visitors</div>
                <div class="mr-5"><?php  echo $this->db->get("visitor")->num_rows() ?> Visitor </div>
              </div>
              
            </div>
          </div>
        </div>

<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Visitor<br>
           
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>IP Address</th>
                    <th>Access From</th>
                    <th>OS</th>
                    <th>Browser</th>
                    <th>Date Access</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>IP Address</th>
                    <th>Access From</th>
                    <th>OS</th>
                    <th>Browser</th>
                    <th>Date Access</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Visitor->show(); 
                    
                    foreach ($cek_query->result_array() as $row)
                    {       
                  ?>
                  <tr>
                     <td><?php cetak($row['ip_address']) ?> </td>
                    <td><?php cetak($row['dari']) ?> </td>
                    <td><?php cetak($row['os']) ?> </td>
                    <td><?php cetak($row['browser']) ?> </td>
                    <td><?php cetak($row['date']) ?> </td>
                    
                  </tr>
                  
                 
                  
           
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

