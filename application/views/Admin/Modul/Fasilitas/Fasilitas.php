<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Fasilitas<br>
           
          </div>
          <div class="card-body">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#manajemen" name=""><i class="fas fa-plus fa-sm"></i> Add data </button>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Icon</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Icon</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php
                  
                  $admin_user = $this->session->userdata('user');
                    $cek_query=$this->Fasilitas->show(); 
                    
                    foreach ($cek_query->result_array() as $row) 
                    {       
                  ?>
                  <tr>
                    <td><?php cetak($row['fasilitas_nama']) ?> </td>
                    <td><?php cetak($row['fasilitas_deskripsi']) ?> </td>
                    <td><img style="width: 100px;" src="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>"></td>

                    <td>
                      <a href="<?php cetak( base_url('Admin/Modul/Fasilitas/Fasilitas/delete?id=') . $row['fasilitas_id'] )?>" class="btn btn-danger"> Delete </a>
                      <a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['fasilitas_id'] )?>">Edit</a>
                      <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#myModalupload<?php cetak( $row['fasilitas_id'] )?>" name=""><i class="fas fa-plus fa-sm"></i> Image activities </button>

                    </td>
                  </tr>
                  
                 <!-- Modal upload -->
                  <div id="myModalupload<?php cetak( $row['fasilitas_id'] )?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <!-- konten modal-->
                      <div class="modal-content">
                        <!-- heading modal -->
                          <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        <!-- body modal -->
                        <div class="modal-body">
                        <form action="<?php cetak( base_url() )?>Admin/Modul/Fasilitas/Fasilitas/uploadImage?id=<?php cetak( $row['fasilitas_id'] )?>"  method="post" enctype="multipart/form-data">
                         <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                            <input type='file' name='userfile[]' class="form-control" multiple> 
                            <button type="submit" class="btn btn-primary btn-block">Insert</button>
                          </form><br><br>
                          <!-- foto -->
                          <div class="row">
                          <?php foreach($this->Fasilitas->show_foto($row['fasilitas_id'])->result_array() as $row_image){ ?>
                            <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
                              <div class="member">
                                <div class="member-img">
                              
                                  <img style="width:230px;height:140px;" src="<?= base_url().'image/fasilitas/'.$row_image['nama_foto'] ?>" class="img-fluid" alt="">
                                  <!-- <div class="social">
                                    <a href=""><i class="icofont-twitter"></i></a>
                                    <a href=""><i class="icofont-facebook"></i></a>
                                    <a href=""><i class="icofont-instagram"></i></a>
                                    <a href=""><i class="icofont-linkedin"></i></a>
                                  </div> -->
                                </div>
                                <div class="portfolio-info">
                                
                                        <a  href="<?php cetak( base_url().'image/fasilitas/'.$row_image['nama_foto'])?>" data-gall="portfolioGallery" class="venobox preview-link" title="<?php cetak($row['fasilitas_nama']) ?>">Zoom<i class="bx bx-plus"></i></a>
                                        <a  href="<?php cetak( base_url('Admin/Modul/Fasilitas/Fasilitas/delete_multiple_foto?id=') . $row_image['id'] )?>" data-gall="portfolioGallery" class="venobox preview-link" title="<?php cetak($row['fasilitas_nama']) ?>">Delete<i class="bx bx-plus"></i></a>
                                </div>
                              </div>  
                            </div>
                          <?php } ?>
                        </div>
                          <!-- end foto -->
                        </div>
                        <!-- footer modal -->
                        <div class="modal-footer">
                          <a href="<?php cetak( base_url('Admin/Modul/Fasilitas/Fasilitas/delete_all_multiple_foto?id=') . $row['fasilitas_id'] )?>" class="btn btn-danger"> Delete All</a>
                        </div>
                      </div>
                    </div>
                  </div>
                   <!-- end Modal upload -->
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['fasilitas_id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <?php
                              
                                $cek_query_edit=$this->Fasilitas->show_where($row['fasilitas_id']); 
                                foreach($cek_query_edit->result_array() as $edit){ ?>


                              <form action="<?php cetak( base_url() )?>Admin/Modul/Fasilitas/Fasilitas/edit"  method="post" enctype="multipart/form-data">
                              <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
 
                              <div class="form-group">
                                  
                                    <label>ID</label>
                                     <input type="text" name="fasilitas_id" class="form-control"  placeholder="Fasilitas" value="<?php cetak( $edit['fasilitas_id'] )?>" required readonly>

 
                                    <label>Name</label>
                                    <input type="text" name="fasilitas_nama" class="form-control" value="<?php cetak( $edit['fasilitas_nama'] )?>" placeholder="Name" required>

                                    <label>Description</label>
                                    <input type="text" name="fasilitas_deskripsi" class="form-control" value="<?php cetak( $edit['fasilitas_deskripsi'] )?>" placeholder="Description" required>
                                    
                                   
                                    <br><br>
                                    <label> Foto </label>  
                                    <br>
                                    <input type="file" class="form-control" name="filefoto" >
                                    <br>

                                    
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                  <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </form>
                              <?php } ?>
                            </div>
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>


        <!-- Modal Add-->
        
  <div class="modal fade" id="manajemen" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?php cetak(base_url())?>Admin/Modul/Fasilitas/Fasilitas/add"  method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
          
          <div class="form-group">
                                  
               <label>Name</label>
              <input type="text" name="fasilitas_nama" class="form-control"  placeholder="Fasility Name" required>

              <label>Description</label>
              <input type="text" name="fasilitas_deskripsi" class="form-control"  placeholder="Description" required>
                 

                                
              <br><br>
              <label> Foto </label>  
              <br>
              <input type="file" class="form-control" name="filefoto" >
              <br>

                   
              </div>
                                  
              <button type="submit" class="btn btn-primary">Tambah</button>
              <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
                              
           </div>
          <div class="modal-footer">

         </div>
        </div>
      </div>