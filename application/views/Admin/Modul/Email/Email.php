<!-- DataTables Example -->
<div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Req Demo Application<br>
           
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                 <tr>
                    <th>Email</th>
                    <th>Nama</th>
                    <th>Tlp</th>
                    <th>Application</th>
                    <th>Date Time</th>
                    <th>Status</th>
                    <th>View Messege</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                  <th>Email</th>
                    <th>Nama</th>
                    <th>Tlp</th>
                    <th>Application</th>
                    <th>Date Time</th>
                    <th>Status</th>
                    <th>View Messege</th>
                  </tr>
                </tfoot>
                <tbody>
                 <?php
                  
                  $admin_user = $this->session->userdata('user');
                 
                    foreach ($this->db->get("pesan")->result_array() as $row)
                    {       
                  ?>
                  <tr>
                     <td><?php cetak($row['dari']) ?> </td>
                    <td><?php cetak($row['nama']) ?> </td>
                    <td><?php cetak($row['no_tlp']) ?> </td>
                    <td><?php cetak($row['aplikasi']) ?> </td>
                    <td><?php cetak($row['datetime']) ?> </td>
                    <td><?php cetak($row['status']) ?> </td>
                    <td><a href="#" type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php cetak( $row['id'] )?>">View Messege</a></td>           
                  </tr>
                  
                 
                  
                

                     <!-- Modal Edit-->
                      <div class="modal fade" id="myModal<?php cetak( $row['id'] )?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>

                            <div class="modal-body">
                                  <p style="text-align:justify;">   <?php echo nl2br(str_replace(' ',' ', htmlspecialchars($row['pesan']))); ?> </p>
                            </div>
                            
                            <div class="modal-footer">

                            </div>
                        </div>
                    </div>

                    <!-- End Modal Edit-->
                    <?php } ?>
                </tbody>
               
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
