<!DOCTYPE html>
<html lang="en">
  <?php require_once('head.php');?>
    
<body>

  <?php require_once('header.php');?>

  <main id="main">

	<!-- Start All Pages -->
  <div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Gallery</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

   	<!-- Start Gallery -->
	<div class="gallery-box about-background" >
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
          <br><h2>Activities</h2>
				
					</div>
				</div>
			</div>
			<div class="tz-gallery">
				<div class="row">
					
          <?php foreach($showing as $row){ ?>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <a class="lightbox" href="#" data-toggle="modal" data-target="#myModalupload<?php cetak( $row['fasilitas_id'] )?>"> 
               <img style="width:450px;height:270px;" src="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>" class="img-fluid" alt="">
            </a>
         
					</div>
          
           <!-- Modal upload -->
           <div id="myModalupload<?php cetak( $row['fasilitas_id'] )?>" class="modal fade " role="dialog">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">     
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php cetak($row['fasilitas_deskripsi']) ?></h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                  <?php $id = $row['fasilitas_id'];  ?> <?= $id ?>
                  <?php if (isset($row['fasilitas_id']) == $id){ ?>
                      <?php foreach($this->db->query("SELECT * from foto join fasilitas on fasilitas_id = get_id where get_id=$id")->result_array() as $row_image){ ?>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                          <a class="lightbox" href="<?php cetak( base_url().'image/fasilitas/'.$row_image['nama_foto'])?>">
                            <img class="img-fluid" src="<?php cetak( base_url().'image/fasilitas/'.$row_image['nama_foto'])?>" >
                          </a>
                        </div>
                      <?php  } ?>
                  <?php } ?>
                    </div>  
                  </div>
                  <div class="modal-footer"> </div>
                </div>
              </div>
            </div>
            <!-- end Modal upload -->
          <?php } ?>
				</div>
      </div>
      <?php 
         echo $this->pagination->create_links();
      ?>
		</div>
	</div>
	<!-- End Gallery -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <?php require_once('footer.php'); ?>
 <!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <?php require_once('vendor_js_files.php'); ?>

</body>

</html>