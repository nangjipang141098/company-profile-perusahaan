<div id="slides" class="cover-slides">
		<ul class="slides-container">
		<?php foreach($this->db->get("image_lp")->result_array() as $row): ?>
			<li class="text-left">
				<img src="<?php cetak( base_url().'image/landing_page/'.$row['foto1'])?>" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 class="m-b-20"><strong><?= $wellcome ?></strong></h1>
							<p class="m-b-40"><?= $wellcome2 ?></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-left">
				<img src="<?php cetak( base_url().'image/landing_page/'.$row['foto2'])?>" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
              <h1 class="m-b-20"><strong><?= $wellcome ?></strong></h1>
							<p class="m-b-40"><?= $wellcome2 ?></p>
						</div>
					</div>
				</div>
			</li>
			<li class="text-left">
				<img src="<?php cetak( base_url().'image/landing_page/'.$row['foto3'])?>" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
               <h1 class="m-b-20"><strong><?= $wellcome ?></strong></h1>
							  <p class="m-b-40"><?= $wellcome2 ?></p>
						</div>
					</div>
				</div>
			</li>
		<?php endforeach; ?>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
	</div>