

<style>
  .video-container {
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px; height: 0; overflow: hidden;
  }
  .video-container iframe,
  .video-container object,
  .video-container embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  }
  .bodysq{
    background-color: #f8f9fa ;
  }
</style>
    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" >
     


     <!-- about-background -->
     <div class="about-section-box bodyw">
        <div class="container">
       
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2><?= $Portfolio ?> & <?= $Testimoni ?> </h2>
					
						
					</div>
				</div>
			
          <div class="row">
            
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
              <div class="inner-column">
              <center><b><h3><?= $Testimoni_description ?></h3></b> </center>
              <div class="col-md-8 mr-auto ml-auto text-center">
					<div id="reviews" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner mt-4">
							    <div class="carousel-item text-center active">
                    <?php foreach($this->Testimoni->show_where_first()->result_array() as $test_first){ ?>
                      <div class="img-box p-1 border rounded-circle m-auto">
                        <img class="d-block w-100 rounded-circle" style="width:130px;height:130px;" src="<?php cetak( base_url().'image/testimoni/'.$test_first['image'] )?>" alt="">
                      </div>
                      
                      <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase"><?php cetak($test_first['dari']) ?> </strong></h5>
                      <h6 class="text-dark m-0"><?php cetak($test_first['aplikasi']) ?></h6>
                      <h4><p class="m-0 pt-3">"<?php cetak($test_first['deskripsi']) ?>"</p></h4>
                      <?php } ?>
                    </div>
                    
                    <?php foreach($this->Testimoni->show_next()->result_array() as $row){ ?>
                    <div class="carousel-item text-center">
                      <div class="img-box p-1 border rounded-circle m-auto">
                        <img class="d-block w-100 rounded-circle" style="width:130px;height:130px;" src="<?php cetak( base_url().'image/testimoni/'.$row['image'] )?>" alt="">
                      </div>
                      <h5 class="mt-4 mb-0"><strong class="text-warning text-uppercase"><?php cetak($row['dari']) ?></strong></h5>
                      <h6 class="text-dark m-0"><?php cetak($row['aplikasi']) ?></h6>
                      <h4><p class="m-0 pt-3">"<?php cetak($row['deskripsi']) ?>"</p></h4>
                    </div>
                    <?php } ?>
						</div>
						<a class="carousel-control-prev" href="#reviews" role="button" data-slide="prev">
							<i class="fa fa-angle-left" aria-hidden="true"></i>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#reviews" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							<span class="sr-only">Next</span>
						</a>
                    </div>
				</div>
         
              
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="inner-column">
              <center><b><h3><?= $product_desciption ?></h3></b> </center>
              <div class="row">
					
          <?php foreach($this->Produk->show()->result_array() as $row1){?>	
            <div class="col-sm-6 col-md-4 col-lg-4">
              <a class="lightbox" href="<?php cetak(base_url()) ?>Product_public/bangga/?id=<?php cetak($row1['id_produk']) ?>" > 
              <center><img style="width:150px;height:150px;" src="<?php cetak( base_url().'image/produk/'.$row1['icon'])?>"  alt=""></center>
              </a>
            </div>
         
              <?php } ?>
    
            
            </div>
              </div>
               
              
				    </div>
          </div>
        </div>
      </div>



        
