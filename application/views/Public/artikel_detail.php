<!DOCTYPE html>
<html lang="en"><!-- Basic -->

<?php require_once('head.php') ?>

<body>
	<!-- Start header -->
	<?php require_once('header.php') ?>
	<!-- End header -->
	
	<!-- Start All Pages -->
<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Article</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	<!-- Start blog details -->
	<div class="blog-box">
		<div class="container">
			
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-12">
					<div class="blog-inner-details-page">
                    <?php foreach($this->News->show_where($this->input->get('id'))->result_array() as $row ): ?>
						<div class="blog-inner-box">
							<div class="side-blog-img">
								<img style="width:709px;height:450px; class="img-fluid" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>" alt="">							
								<div class="date-blog-up">
									<?php cetak($row['berita_waktu']) ?>
								</div>
							</div>
							<div class="inner-blog-detail details-page">
								<h3><?php cetak($row['berita_judul']) ?></h3>
								<ul>
									<li><i class="zmdi zmdi-account"></i>Posted By : <span><?php cetak($row['admin_nama']) ?></span></li>
									<li>|</li>
									<li><i class="zmdi zmdi-time"></i>Time : <span><?php cetak($row['berita_waktu']) ?></span></li>
									<li>|</li>
									<li><i class="zmdi zmdi-seen"></i>Viewed <span><?php cetak($row['seen']) ?> times</span></li>
								</ul>
								<p style="text-align:justify;">   <?php echo nl2br(str_replace(' ',' ', htmlspecialchars($row['berita_deskripsi']))); ?> </p>
								
							</div>
						</div>
                    <?php endforeach; ?>
					
						<div class="blog-comment-box">
							
							
							
						</div>
						
					</div>
				</div>
			
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-12 blog-sidebar">
					<div class="right-side-blog">
						
						<h3>Recent Post</h3>
						<div class="post-box-blog">
							<div class="recent-post-box">
							<?php foreach($this->News->show_where_news(0,7)->result_array() as $row){?>	
								<div class="recent-box-blog">
									<div class="recent-img">
										<img class="img-fluid" src="images/post-img-02.jpg" alt="">
									</div>
									<div class="recent-info">
										<ul>
											<li><i class="zmdi zmdi-account"></i>Posted By : <span><?php cetak($row['admin_nama']) ?></span></li>
											<li>|</li>
											<li><i class="zmdi zmdi-time"></i>Time : <span><?php cetak($row['berita_waktu']) ?></span></li>
										</ul>
										<h4><?php cetak($row['berita_judul']) ?></h4>
                                        <p style="text-align:justify;"><?php cetak(substr($row['berita_deskripsi'], 0, 80)) ?><a class="btn-warning" href="<?php base_url() ?>Artikel?lang=english&id=<?php cetak($row['berita_id']) ?>">Read More</a></p>
									</div>
								</div>
							<?php } ?>
							
							<a class="btn btn-lg btn-circle btn-outline-new-white" href="<?= site_url() ?>Welcome/news_page">Read More</a>   
							</div>
						</div>
						
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!-- End details -->
	
	<!-- Start Footer -->
	<?php require_once('footer.php') ?>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

	<!-- ALL JS FILES -->
	<?php require_once('vendor_js_files.php') ?>
</body>
</html>