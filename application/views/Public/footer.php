
  <section id="contact">
  <!-- ======= Footer ======= -->
  <footer class="footer-area aboutt-background">
  <?php foreach($this->Identitas->show()->result_array() as $row): ?>
		<div class="container">
			<div class="row">
				
				<div class="col-lg-3 col-md-6">
					<h3>Subscribe</h3>
					<ul class="list-inline f-social">
					<li class="list-inline-item"><a href="https://wa.me/6283876678524?text="><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="https://web.facebook.com/trinindyautama?_rdc=1&_rdr"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="https://id.linkedin.com/in/tri-nindya-utama-402904138"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<li class="list-inline-item"><a href="<?php cetak($row['identitas_gp']) ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					
					</ul>
				</div>
				<div class="col-lg-8 col-md-6">
					<h3>Contact information</h3>
					<p class="lead"><?php cetak($row['identitas_alamat']) ?></p>
					<p class="lead"><?php cetak($row['identitas_notelp']) ?></p>
					<p> <?php cetak($row['identitas_email']) ?></p>
				</div>
				
			</div>
		</div>
	<?php endforeach; ?>
		
		<div class="copyright">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<p class="company-name"> PT. TRI NINDYA UTAMA &copy; 2020  </p>
					</div>
				</div>
			</div>
		</div>
		
	</footer><!-- End Footer -->
  </section>