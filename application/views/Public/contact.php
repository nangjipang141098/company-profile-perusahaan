

    <?php foreach($this->Identitas->show()->result_array() as $row){ ?>
    <section id="contact" class="services ">
      <div class="section-title" data-aos="fade-up">
          <h2><?= $contact ?></h2>
          <p> </p>
        </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-5" data-aos="fade-up">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-map"></i></div>
              <p class="description"><h5><?php cetak($row['identitas_alamat'])?></h5></p>
            </div>
          </div>
          <div class="col-lg-4 col-md-5" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i  class="bx bx-envelope"></i></div>
              <p class="description"><h5><?php cetak($row['identitas_email'])?></h5></p>
            </div>
          </div>
          <div class="col-lg-4 col-md-5" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-phone-call"></i></div>
              <p class="description"><h5><?php cetak($row['identitas_notelp'])?></h5></p>
            </div>
          </div>
          <div class="col-lg-12 col-md-8" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box">
            <div class="icon"><i class="bx bx-map"></i></div>
              <p class="description"><iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.365707919432!2d106.72501491429315!3d-6.2154080955006785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f7205ad9e899%3A0x95cf7e71b8fd086b!2sTri%20Nindya%20Utama%20(Bangga%20Solution)!5e0!3m2!1sid!2sid!4v1610261599241!5m2!1sid!2sid" width="1000" height="120" frameborder="3" style="border:3;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></p>
            </div>
          </div>
       

        </div>
      </div>
    </section>

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

      
        
      </div>
    </section><!-- End Contact Section -->

    <?php } ?>