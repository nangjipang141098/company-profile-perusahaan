<section id="artikel">
	<div class="blog-box srv-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2><?= $latest_update_section ?></h2>
					</div>
				</div>
			</div>
			<div class="row">
				
			<?php foreach($this->News->show_where_news(0,3)->result_array() as $row){?>
				<div class="col-lg-4 col-md-6 col-12">
					<div class="blog-box-inner">
						<div class="blog-img-box">
							<img class="img-fluid" src="<?= cetak(base_url()) ?>template/template_public/assets/images/white.jpg" style="height:280px;" alt="">
						</div>
						<div class="blog-detail">
							<h4><?php cetak($row['berita_judul']) ?></h4>
							<ul>
								<li><span>Post by <?php cetak($row['admin_nama']) ?></span></li>
								<li>|</li>
								<li><span><?php cetak($row['berita_waktu']) ?></span></li>
							</ul>
							<p><?php cetak(substr($row['berita_deskripsi'], 0, 120)) ?><a class="btn-warning" href="<?php site_url() ?>Artikel?lang=english&id=<?php cetak($row['berita_id']) ?>">Read More</a></p>
						</div>
					</div>
				</div>
			<?php } ?>
				
			</div>
			<?php if($this->News->show_where_news(0, 3)->num_rows() > 3 ){ ?>
				<center><a class="btn btn-lg btn-circle btn-outline-new-white" href="<?= base_url() ?>Dashboard/Dashboard/galery_page">Show All</a></center>
            <?php } ?>
		
		</div>
	</div>
	</section>