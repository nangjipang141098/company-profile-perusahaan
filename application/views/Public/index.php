<!DOCTYPE html>
<html lang="en"><!-- Basic -->

<?php require_once('head.php') ?>

<body>
	<!-- Start header -->
	<?php require_once('header_public.php') ?>
	<!-- End header -->
	
	<!-- Start slides -->
	<?php require_once('hero_section.php') ?>
	<!-- End slides -->
	
	<!-- Start About -->
	<?php require_once('about_section.php') ?>
	<!-- End About -->
	
	<?php require_once('visitor.php') ?>
	
	<!-- Start service -->
	<?php require_once('service_section.php') ?>
	<!-- End service -->

	<!-- Start service -->
	<?php // require_once('product_section.php') ?>
	<!-- End service -->
					
		<!-- Start Customer Reviews -->
		<?php require_once('testimonial_section.php') ?>
		<!-- End Customer Reviews -->
		<?php require_once('all_product.php')?>
	<!-- Start Gallery -->
	<?php require_once('galeri.php') ?>
	<!-- End Gallery -->

	 <!-- ======= PRODUK Section ======= -->
	<?php require_once('all_product.php')?>

	
	<!-- Media service -->
	<?php // require_once('latest_update_section.php') ?>
	<!-- Media service -->

	
	<!-- Carrer service -->
	<?php //require_once('jobs.php') ?>
	<!-- Carrer service -->
	
	<iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.365707919432!2d106.72501491429315!3d-6.2154080955006785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f7205ad9e899%3A0x95cf7e71b8fd086b!2sTri%20Nindya%20Utama%20(Bangga%20Solution)!5e0!3m2!1sid!2sid!4v1610261599241!5m2!1sid!2sid" class="map-full"></iframe>
	
	
	<!-- End Contact info -->
	
	<!-- Start Footer -->
	<?php require_once('footer.php') ?>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

	<!-- ALL JS FILES -->
	<?php require_once('vendor_js_files.php') ?>
</body>
</html>