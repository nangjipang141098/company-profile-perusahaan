<!DOCTYPE html>
<html lang="en"><!-- Basic -->

<?php require_once('head.php') ?>

<body>
	
	<!-- Start header -->
	<?php require_once('header.php') ?>
	<!-- End header -->
	<?php foreach($this->Produk->show_where($this->input->get('id'))->result_array() as $row): ?>
	<!-- Start All Pages -->
<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1><?php cetak($row['nama_produk']) ?></h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
	
	<!-- Start blog details -->
	<div class="blog-box">
		<div class="container">
			
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-12">
					<div class="blog-inner-details-page">
                  
						<div class="blog-inner-box">
							<div class="side-blog-img">
								<img style="width:709px;height:450px; class="img-fluid" src="<?php cetak( base_url().'image/produk/'.$row['image'])?>" alt="">							
							</div>
							<div class="inner-blog-detail details-page">
								<?php foreach($this->db->get("language")->result_array() as $language): ?>
								<h3><?php //cetak($row['berita_judul']) ?></h3>
								<?php if($language['language'] == 'indo'){ ?>
									<p style="text-align:justify;">   <?php echo nl2br(str_replace(' ',' ', htmlspecialchars($row['deskripsi_indo']))); ?> </p>
								<?php }  else if($language['language']  == 'english'){ ?>
									<p style="text-align:justify;">   <?php echo nl2br(str_replace(' ',' ', htmlspecialchars($row['deskripsi_ing']))); ?> </p>
								<?php } ?>
								<?php endforeach; ?>
							</div>
						</div>
                   
						<div class="blog-comment-box">
							
							
							
						</div>
						
					</div>
				</div>
			
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-8 col-12 blog-sidebar">
					<div class="right-side-blog">
						
						<h3>Project</h3>
						<div class="post-box-blog">
							<div class="recent-post-box">
							<?php foreach($this->Partner->show_where_id($this->input->get('id'))->result_array() as $row1){?>	
								<div class="recent-box-blog">
								
									<a href="<?php cetak($row1['mitra_link']) ?>"> <img style="width: 100px;" src="<?php cetak( base_url().'image/partner/'.$row1['mitra_gambar'])?>"></a>
								
									
									
								</div>
							<?php } ?>
							<br>
							<a class="btn btn-lg btn-circle btn-outline-new-white" href="<?php cetak( base_url().'image/produk/'.$row['dokumen'] )?>">Download Brosur</a>   
							</div>
							<br><br>
							<h3>Other Procuct</h3>
							<div class="recent-post-box">
							<?php foreach($this->Produk->show()->result_array() as $row1){?>	
								
									<a href="<?php cetak(base_url()) ?>Product_public/bangga?lang=indo&id=<?php cetak($row1['id_produk']) ?>">  <img style="width: 100px;" src="<?php cetak( base_url().'image/produk/'.$row1['icon'])?>"></a>
									
							<?php } ?>
							<br>

							</div>
						</div>
						
					</div>
				</div>

				
			
			</div>
		</div>
	</div>
	<!-- End details -->

	<?php require_once('request_documentation.php') ?>	
	<!-- Start Footer -->
	<?php require_once('footer.php') ?>
	<!-- End Footer -->
	
	<a href="#" id="back-to-top" title="Back to top" style="display: none;"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>

	<!-- ALL JS FILES -->
	<?php require_once('vendor_js_files.php') ?>
	<?php endforeach; ?>
</body>
</html>