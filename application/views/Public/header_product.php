<header class="top-navbar">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">
					<?php foreach($this->db->get("identitas")->result_array() as $row): ?>
					<img style="width: 70px;" src="<?php base_url() ?>image/identitas/<?= $row['identitas_favicon'] ?>" alt="" />
					<?php endforeach ?>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-rs-food" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbars-rs-food">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="<?= base_url() ?>#" id="dropdown-a" data-toggle="dropdown"><?= $Drop_down_corporate ?></a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="<?= base_url() ?>welcome/#about"><?= $About ?></a>
									<a class="dropdown-item" href="<?= base_url() ?>Welcome/struktur"><?= $organization_structure ?></a>
								<a class="dropdown-item" href="<?= base_url() ?>Welcome/Carrer"><?= $Career ?></a>
							</div>
						</li>
      				    <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="<?= base_url() ?>#" id="dropdown-a" data-toggle="dropdown"><?= $Drop_down_pns ?></a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item" href="#services"><?= $Services ?></a>
									
									<!-- <a class="dropdown-item" href="<?= base_url() ?>Product_public/bangga/english#req_documentation"><?= $request_documentation ?></a> -->
									<a class="dropdown-item" href="<?= base_url() ?>Welcome/#testimonials"><?= $Testimoni ?> & <?= $Portfolio ?></a>
								</a>					
							</div>
							</li>
        			    <li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="<?= base_url() ?>#" id="dropdown-a" data-toggle="dropdown"><?= $Drop_down_article_media ?></a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
								<a class="dropdown-item"  href="<?= base_url()?>Welcome/#galery"><?= $Galery ?></a>
              					  <a class="dropdown-item"  href="<?= base_url()?>Welcome/news_page"><?= $Drop_down_article_media ?></a>
							</div>
						</li>
          	<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="<?= base_url() ?>#" id="dropdown-a" data-toggle="dropdown"><?= $Drop_down ?></a>
							<div class="dropdown-menu" aria-labelledby="dropdown-a">
							    	<a class="dropdown-item" href="<?= base_url() ?>Product_public/bangga/?lang=indo">Indonesia</a>
              						<a class="dropdown-item" href="<?= base_url() ?>Product_public/bangga/?lang=english">English</a>
							</div>
						</li>
						<li class="nav-item"><a class="nav-link" href="<?= base_url()?>Welcome/#contact"><?= $contact ?></a></li>
					</ul>
				</div>
			</div>
		</nav>
	</header>