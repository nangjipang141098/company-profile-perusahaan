

<style>
  .video-container {
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px; height: 0; overflow: hidden;
  }
  .video-container iframe,
  .video-container object,
  .video-container embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  }
  .body-galeri{
    background-color: #f1f3f4;
  }
  </style>


<section id="galery">
  <!-- Start Gallery -->
  <!-- qqt-background -->
	<div class="gallery-box body-galeri"> 
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
            <h2><?= $Galery ?></h2>
            <h4><?= $galery_description ?></h4>     
					</div>
				</div>
			</div>
			<div class="tz-gallery">
				<div class="row">
					
          <?php foreach($this->Fasilitas->show_limit(0,4)->result_array() as $row){ ?>
					<div class="col-sm-6 col-md-4 col-lg-3">
            <a class="lightbox" href="<?php cetak(base_url()) ?>Welcome/galery_page?id_galery=<?php cetak($row['fasilitas_id']) ?>" data-rel="popup" data-position-to="window" data-transition="fade"> 
               <img style="width:450px;height:200px;" src="<?php cetak( base_url().'image/fasilitas/'.$row['fasilitas_gambar'])?>" class="img-fluid" alt="">
            </a>

         

               
          </div>
          
           
                  
          
        
          <?php } ?>


		  
		  <?php if($this->Fasilitas->show_limit(0,8)->num_rows() >=8 ){ ?>
			<a class="btn btn-lg btn-circle btn-outline-new-white" href="<?= base_url() ?>Welcome/galery_page/8">Read More</a>   
		  <?php } ?>

          
				
				</div>
			</div>
		</div>
	</div>
	<!-- End Gallery -->

</section>



