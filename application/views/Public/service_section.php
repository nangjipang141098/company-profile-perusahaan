<style>
  .video-containerr {
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px; height: 0; overflow: hidden;
  }
  .video-containerr iframe,
  .video-containerr object,
  .video-containerr embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  }
  .body{
	  background-color: #f8f9fa ;
  }
</style>


<section id="services">
<!-- srv-background -->
<div class="blog-box body">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2><?= $Services ?></h2>
						<h4><?= $service_devinition ?></h4>
					</div>
				</div>
			</div>
			<div class="row">
				
		
				<div class="col-lg-4 col-md-6 col-12">
					<div class="blog-box-inner">
						<div class="blog-img-box">
							<img class="video-containerr" src="<?= cetak(base_url()) ?>template/template_public/assets/images/white.jpg" style="height:400px;" alt="">
						</div>
						<div class="blog-detail">
							<center><img src="<?= cetak(base_url()) ?>template/template_public/assets/images/logo-service1.png" style="height:90px;" ></center>
							<center><h2>.---------------------------.</h2></center>
							<center><h2><?= $app_development ?> </h2></center>
							<p><?= $type_app_develompemnt ?></p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<div class="blog-box-inner">
						<div class="blog-img-box">
							<img class="video-containerr" src="<?= cetak(base_url()) ?>template/template_public/assets/images/white.jpg" style="height:400px;" alt="">
						</div>
						<div class="blog-detail">
							<center><img src="<?= cetak(base_url()) ?>template/template_public/assets/images/logo-service2.png" style="height:90px;" ></center>
							<center><h2>.---------------------------.</h2></center>
							<center><h2><?= $infrastructure_support ?> </h2></center>
							<p><?= $type_infrastructure_support ?></p>
						
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-12">
					<div class="blog-box-inner">
						<div class="blog-img-box">
							<img class="video-containerr" src="<?= cetak(base_url()) ?>template/template_public/assets/images/white.jpg" style="height:400px;" alt="">
						</div>
						<div class="blog-detail">
							<center><img src="<?= cetak(base_url()) ?>template/template_public/assets/images/logo-service3.png" style="height:90px;" ></center>
							<center><h2>.---------------------------.</h2></center>
							<center><h2><?= $staffing_service_models ?> </h2></center>
							<p><?= $type_staffing_service_models ?></p>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>

</section>