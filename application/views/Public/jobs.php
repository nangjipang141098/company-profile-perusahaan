<div class="blog-box carrer-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2><?= $Career?></h2>
						
					</div>
				</div>
			</div>
			<div class="row">
				
			<?php foreach($this->News->show_where_jobs(0,3)->result_array() as $row){?>
				<div class="col-lg-4 col-md-6 col-12">
					<div class="blog-box-inner">
						<div class="blog-img-box">
							<img class="img-fluid" src="<?= cetak(base_url()) ?>template/template_public/assets/images/white.jpg" style="height:280px;" alt="">
						</div>
						<div class="blog-detail">
						<h4><?php cetak($row['berita_judul']) ?></h4>
							<ul>
								<li><span>Post by <?php cetak($row['admin_nama']) ?></span></li>
								<li>|</li>
								<li><span><?php cetak($row['berita_waktu']) ?></span></li><br>
							</ul>
							<p><?php cetak(substr($row['berita_deskripsi'], 0, 150)) ?></p>
							<center><button type="button" class="btn btn-lg btn-circle btn-outline-new-white" data-toggle="modal" data-target="#myModal<?php cetak($row['berita_id']) ?>">Detail</button></center>
						</div>
					</div>
				</div>

				<!-- Modal carrer -->
				<div id="myModal<?php cetak($row['berita_id']) ?>" class="modal fade" role="dialog">
					<div class="modal-dialog">
					
						<!-- konten modal-->
						<div class="modal-content">
							<!-- heading modal -->
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"></h4>
							</div>
							<!-- body modal -->
							<div class="modal-body">
							<h4><?php cetak($row['berita_judul']) ?></h4>
							<img class="img-fluid" src="<?php cetak( base_url().'image/news/'.$row['berita_gambar'])?>" alt="">
								<p style="text-align:justify;">   <?php echo nl2br(str_replace(' ',' ', htmlspecialchars($row['berita_deskripsi']))); ?> </p>
							</div>
							<!-- footer modal -->
							<div class="modal-footer">
								<!-- footer modal -->
							</div>
						</div>
					</div>
				</div>
				<!-- end Modal carrer -->






				<?php } ?>
			
				
			</div>
		</div>
	</div>