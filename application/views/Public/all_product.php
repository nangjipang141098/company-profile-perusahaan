<!-- Start Menu -->
<div class="menu-box ">
		<div class="container">
			
		<div class="heading-title text-center">
            <h2><?= $Client ?></h2>
            <h4><?= $Client_description ?></h4>     
					</div>
			<div class="row inner-menu-box">
				
				<div class="col-3">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Bangga E-Tila</a>
						<a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Bangga E-Risk</a>
						<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Bangga WBS</a>
						<a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Bangga KMS</a>
						<a class="nav-link" id="v-pills-settingss-tab" data-toggle="pill" href="#v-pills-settingss" role="tab" aria-controls="v-pills-settingss" aria-selected="false">Bangga E-Office</a>
						<a class="nav-link" id="v-pills-settingsss-tab" data-toggle="pill" href="#v-pills-settingsss" role="tab" aria-controls="v-pills-settingsss" aria-selected="false">Bangga KPKU</a>
					</div>
				</div>
				
				<div class="col-9">
					<div class="tab-content" id="v-pills-tabContent">
						<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
							<div class="row">
							<?php foreach($this->Partner->show_where_id(1)->result_array() as $etila): ?>
								<div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<center><img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="Image"></center>
										<div class="why-text">
											<h4><?php cetak($etila['mitra_nama']) ?></h4>
											
											<h5><a class="btn  btn-block btn-circle btn-outline-new-white" href=<?php cetak($etila['mitra_link']) ?>>Visit Here</a></h5>
										</div>
									</div>
								</div>
							<?php endforeach; ?>	
								
							</div>
							
						</div>
						<div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
							<div class="row">
							<?php foreach($this->Partner->show_where_id(2)->result_array() as $etila): ?>
								<div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<center><img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="Image"></center>
										<div class="why-text">
											<h4><?php cetak($etila['mitra_nama']) ?></h4>
											
											<h5><a class="btn btn-block btn-circle btn-outline-new-white" href=<?php cetak($etila['mitra_link']) ?>>Visit Here</a></h5>
										</div>
									</div>
								</div>
							<?php endforeach; ?>	
							</div>
							
						</div>
						<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
							<div class="row">
							<?php foreach($this->Partner->show_where_id(3)->result_array() as $etila): ?>
								<div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<center><img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="Image"></center>
										<div class="why-text">
											<h4><?php cetak($etila['mitra_nama']) ?></h4>
											
											<h5><a class="btn  btn-block btn-circle btn-outline-new-white">Visit Here</a></h5>
										</div>
									</div>
								</div>
							<?php endforeach; ?>	
							</div>
						</div>
						<div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
							<div class="row">
							<?php foreach($this->Partner->show_where_id(4)->result_array() as $etila): ?>
								<div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<center><img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="Image"></center>
										<div class="why-text">
											<h4><?php cetak($etila['mitra_nama']) ?></h4>
											
											<h5><a class="btn  btn-block btn-circle btn-outline-new-white" href=<?php cetak($etila['mitra_link']) ?>>Visit Here</a></h5>
										</div>
									</div>
								</div>
							<?php endforeach; ?>	
							</div>
						</div>
						<div class="tab-pane fade" id="v-pills-settingss" role="tabpanel" aria-labelledby="v-pills-settingss-tab">
							<div class="row">
							<?php foreach($this->Partner->show_where_id(5)->result_array() as $etila): ?>
								<div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<center>
											<img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="Image">
										</center>
										<div class="why-text">
											<h4><?php cetak($etila['mitra_nama']) ?></h4>
											
											<h5><a class="btn  btn-block btn-circle btn-outline-new-white" href=<?php cetak($etila['mitra_link']) ?>>Visit Here</a></h5>
										</div>
									</div>
								</div>
							<?php endforeach; ?>	
							</div>
						</div>
						<div class="tab-pane fade" id="v-pills-settingsss" role="tabpanel" aria-labelledby="v-pills-settingsss-tab">
							<div class="row">
							<?php foreach($this->Partner->show_where_id(6)->result_array() as $etila): ?>
								<div class="col-lg-4 col-md-6 special-grid drinks">
									<div class="gallery-single fix">
										<center>
										<img src="<?php cetak( base_url().'image/partner/'.$etila['mitra_gambar'])?>" class="img-fluid" alt="Image">
										</center>
										<div class="why-text">
											<h4><?php cetak($etila['mitra_nama']) ?></h4>
											
											<h5><a class="btn  btn-block btn-circle btn-outline-new-white" href=<?php cetak($etila['mitra_link']) ?>>Visit Here</a></h5>
										</div>
									</div>
								</div>
							<?php endforeach; ?>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Menu -->

