

<style>
  .video-container {
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 30px; height: 0; overflow: hidden;
  }
  .video-container iframe,
  .video-container object,
  .video-container embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  }
  .body-galeri{
    background-color: #f1f3f4;
  }
  </style>


<section id="galery">
  <!-- Start Gallery -->
  <!-- qqt-background -->
	<div class="gallery-box body-galeri"> 
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
            <h2><?= $Galery ?></h2>
            <h4><?= $galery_description ?></h4>     
					</div>
				</div>
			</div>
			<div class="tz-gallery">
				<div class="row">
					
          <?php foreach($this->Fasilitas->show_limit(0,8)->result_array() as $row){ ?>
					<div class="col-sm-6 col-md-4 col-lg-3">
          <button class="btn btn-sm btn-success mb-2 ml-3" data-toggle="modal" data-target="#myModalupload<?php cetak( $row['fasilitas_id'] )?>" name=""><i class="fas fa-plus fa-sm"></i> Image activities </button>

            <div data-role="popup" id="bukafoto<?php cetak( $row['fasilitas_id'] )?>">
           
            <?php $id = $row['fasilitas_id'];  ?> <?= $id ?>
                    <?php foreach($this->db->query("SELECT * from foto join fasilitas on fasilitas_id = get_id where get_id=$id ")->result_array() as $roww_image){ ?>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <a class="lightbox" href="<?= base_url().$roww_image['direktori'].'/'.$roww_image['nama_foto'] ?>">
                                <img class="img-fluid" src="<?= base_url().$roww_image['direktori'].'/'.$roww_image['nama_foto'] ?>" alt="Gallery Images">
                            </a>
                        </div>
                    <?php } ?>
    
            </div>

             <!-- Modal upload -->
             <div id="myModalupload<?php cetak( $row['fasilitas_id'] )?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <!-- konten modal-->
                      <div class="modal-content">
                        <!-- heading modal -->
                          <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                        <!-- body modal -->
                        <div class="modal-body">
                        <form action="<?php cetak( base_url() )?>Admin/Modul/Fasilitas/Fasilitas/uploadImage?id=<?php cetak( $row['fasilitas_id'] )?>"  method="post" enctype="multipart/form-data">
                         <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none"> 
                            <input type='file' name='userfile[]' class="form-control" multiple> 
                            <button type="submit" class="btn btn-primary btn-block">Insert</button>
                          </form><br><br>
                          <!-- foto -->
                          <div class="row">
                          <?php foreach($this->Fasilitas->show_foto($row['fasilitas_id'])->result_array() as $row_image){ ?>
                            
                            <?php if($row['fasilitas_id'] == $row_image['get_id'])  ?>
                            <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
                              <div class="member">
                                <div class="member-img">

                                <?php $dir = $row['direktori'].'/'.$row_image['nama_foto'];?>
                                <?= $dir ?>
                                  <img style="width:230px;height:140px;" src="<?= base_url().$dir ?>" class="img-fluid" alt="">
                                  <!-- <div class="social">
                                    <a href=""><i class="icofont-twitter"></i></a>
                                    <a href=""><i class="icofont-facebook"></i></a>
                                    <a href=""><i class="icofont-instagram"></i></a>
                                    <a href=""><i class="icofont-linkedin"></i></a>
                                  </div> -->
                                </div>
                                <div class="portfolio-info">
                                
                                        <a  href="<?= base_url().$dir ?>" data-gall="portfolioGallery" class="lightbox" title="<?php cetak($row['fasilitas_nama']) ?>">Zoom<i class="bx bx-plus"></i></a>
                                        <a  href="<?php cetak( base_url('Admin/Modul/Fasilitas/Fasilitas/delete_multiple_foto?id=') . $row_image['id'] )?>" data-gall="portfolioGallery" class="venobox preview-link" title="<?php cetak($row['fasilitas_nama']) ?>">Delete<i class="bx bx-plus"></i></a>
                                </div>
                              </div>  
                            </div>
                          <?php } ?>
                        </div>
                          <!-- end foto -->
                        </div>
                        <!-- footer modal -->
                        <div class="modal-footer">
                          <a href="<?php cetak( base_url('Admin/Modul/Fasilitas/Fasilitas/delete_all_multiple_foto?id=') . $row['fasilitas_id'] )?>" class="btn btn-danger"> Delete All</a>
                        </div>
                      </div>
                    </div>
                  </div>
                   <!-- end Modal upload -->

               
          </div>
          
           
                  
          
        
          <?php } ?>


		  
		  <?php if($this->Fasilitas->show_limit(0,8)->num_rows() >=8 ){ ?>
			<a class="btn btn-lg btn-circle btn-outline-new-white" href="<?= base_url() ?>Welcome/galery_page/8">Read More</a>   
		  <?php } ?>

          
				
				</div>
			</div>
		</div>
	</div>
	<!-- End Gallery -->

</section>