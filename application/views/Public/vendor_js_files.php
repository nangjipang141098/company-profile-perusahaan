<!-- ALL JS FILES -->
	<script src="<?= base_url() ?>template/template_public/assets/js/jquery-3.2.1.min.js"></script>
	<script src="<?= base_url() ?>template/template_public/assets/js/popper.min.js"></script>
	<script src="<?= base_url() ?>template/template_public/assets/js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
	<script src="<?= base_url() ?>template/template_public/assets/js/jquery.superslides.min.js"></script>
	<script src="<?= base_url() ?>template/template_public/assets/js/images-loded.min.js"></script>
	<script src="<?= base_url() ?>template/template_public/assets/js/isotope.min.js"></script>
	<script src="<?= base_url() ?>template/template_public/assets/js/baguetteBox.min.js"></script>
	<script src="<?= base_url() ?>template/template_public/assets/js/form-validator.min.js"></script>
    <script src="<?= base_url() ?>template/template_public/assets/js/contact-form-script.js"></script>
    <script src="<?= base_url() ?>template/template_public/assets/js/custom.js"></script>