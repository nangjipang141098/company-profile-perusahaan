<?php require_once('head.php') ?>
<?php $id = $this->input->get('id_galery') ?>
<div id="slides" class="cover-slides">
		<ul class="slides-container">
		<?php foreach($this->db->from('foto')->join('fasilitas', 'fasilitas.fasilitas_id=foto.get_id')->where('get_id', $id)->get()->result_array() as $row): ?>
			<li class="text-left">
				<img src="<?php cetak( base_url().'image/fasilitas/'.$row['nama_foto'])?>" alt="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							
                            <p class="m-b-40"><?php cetak($row['fasilitas_nama'])?><br>
                            <?php cetak($row['fasilitas_deskripsi'])?>
                        </p>
						</div>
					</div>
				</div>
			</li>
		
		<?php endforeach; ?>
		</ul>
		<div class="slides-navigation">
			<a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
			<a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
		</div>
    </div>
    


    
    <?php require_once('vendor_js_files.php') ?>
