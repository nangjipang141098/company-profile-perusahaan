<!DOCTYPE html>
<html lang="en">
  <?php require_once('head.php');?>

<body>

  <?php require_once('header_product.php');?>


  <main id="main">
        

<!-- Content Row -->

  <!-- Start All Pages -->
<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Article</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->

<!-- Media service -->
<div class="blog-box srv-background">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="heading-title text-center">
						<h2><?= $latest_update_section ?></h2>
					</div>
				</div>
			</div>
			<div class="row">
				
      <?php foreach($showing as $row){?>
				<div class="col-lg-4 col-md-6 col-12">
					<div class="blog-box-inner">
						<div class="blog-img-box">
							<img class="img-fluid" src="<?= cetak(base_url()) ?>template/template_public/assets/images/white.jpg" style="height:280px;" alt="">
						</div>
						<div class="blog-detail">
							<h4><?php cetak($row['berita_judul']) ?></h4>
							<ul>
								<li><span>Post by <?php cetak($row['admin_nama']) ?></span></li>
								<li>|</li>
								<li><span><?php cetak($row['berita_waktu']) ?></span></li>
							</ul>
							<p><?php cetak(substr($row['berita_deskripsi'], 0, 120)) ?><a class="btn-warning" href="<?php base_url() ?>Artikel?lang=english&id=<?php cetak($row['berita_id']) ?>">Read More</a></p>
						</div>
					</div>
				</div>
			<?php } ?>
				
			</div>
      <?php 
         echo $this->pagination->create_links();
      ?>

		
		</div>
	</div>
	<!-- Media service -->

    
  </main><!-- End #main -->

  
  <?php require_once('footer.php') ?>

  <a href="<?= base_url() ?>#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <?php require_once('vendor_js_files.php') ?>
</body>

</html>