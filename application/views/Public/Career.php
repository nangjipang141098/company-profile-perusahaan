<!DOCTYPE html>
<html lang="en">
  <?php require_once('head.php');?>

<body>

  <?php require_once('header_product.php');?>


  <main id="main">
        

<!-- Content Row -->

  <!-- Start All Pages -->
<div class="all-page-title page-breadcrumb">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12">
					<h1>Article</h1>
				</div>
			</div>
		</div>
	</div>
	<!-- End All Pages -->
        <?php require_once('jobs.php') ?>
	<!-- Media service -->

    
  </main><!-- End #main -->

  
  <?php require_once('footer.php') ?>

  <a href="<?= base_url() ?>#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <?php require_once('vendor_js_files.php') ?>
</body>

</html>