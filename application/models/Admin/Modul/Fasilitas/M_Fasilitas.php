<?php

class M_Fasilitas extends CI_Model{

    protected $table = "fasilitas";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function Multiple_upload($data){
        $this->db->insert('foto',$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_foto($where){
        $this->db->select('*');
        $this->db->from('foto');
        $this->db->where('get_id', $where);
		$query = $this->db->get();
        return $query;
    }

    
    function delete_multiple_foto($where)
	{
		$this->db->where('id', $where);
		$this->db->delete('foto');
    }

    function delete_all_multiple_foto($where)
	{
		$this->db->where('get_id', $where);
		$this->db->delete('foto');
    }


    function show_limit($start, $limit){
        $query = $this->db->query("SELECT * FROM $this->table ORDER BY fasilitas_waktu ASC LIMIT $start,$limit");
        return $query;
    } 

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('fasilitas_id', $where);
		$query = $this->db->get();
        return $query;
    }

    function show_page($number, $offset){
		$query = $this->db->get($this->table, $number, $offset)->result_array();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('fasilitas_id', $where);
		$this->db->delete($this->table);
    }


  

    
}