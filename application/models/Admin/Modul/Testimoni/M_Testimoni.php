<?php

class M_Testimoni extends CI_Model{

    protected $table = "testimoni";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('id', $where);
		$query = $this->db->get();
        return $query;
    }

    function show_where_first(){
        $query = $this->db->query("SELECT * FROM $this->table ORDER BY id ASC LIMIT 0,1");
        return $query;
    }

    function show_next(){
        $query_row = $this->db->query("SELECT * FROM $this->table")->num_rows();
        $query = $this->db->query("SELECT * FROM $this->table ORDER BY id ASC LIMIT 1,$query_row");
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('id', $where);
		$this->db->delete($this->table);
    }


  

    
}