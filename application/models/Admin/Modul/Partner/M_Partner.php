<?php

class M_Partner extends CI_Model{

    protected $table = "mitra_kerja";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('mitra_id', $where);
		$query = $this->db->get();
        return $query;
    }

    function show_where_id($where){ //get id to field jenis produck at table mitra kerja
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('jenis_produk', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('mitra_id', $where);
		$this->db->delete($this->table);
    }


  

    
}