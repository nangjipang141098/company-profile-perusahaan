<?php

class M_Hak_akses extends CI_Model{
  
    protected $table = "hak_akses";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('hak_akses_id', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('hak_akses_id', $where);
		$this->db->delete($this->table);
    }

    //akses level code pada modal edit
    function level_code()
	{
		$this->db->select('*');
        $this->db->from('admin_level');
       
		$query = $this->db->get();
        return $query;
    }

    //status activaction account
    function admin_status()
	{
		$this->db->select('*');
        $this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_menu(){
        $this->db->select('*');
		$this->db->from('menu');
		$query = $this->db->get();
        return $query;
    }

    function get_menu_where($where){
        $query = $this->db->query("SELECT menu_nama FROM menu WHERE menu_level=$where");
        return $query;
    }

  

    
}