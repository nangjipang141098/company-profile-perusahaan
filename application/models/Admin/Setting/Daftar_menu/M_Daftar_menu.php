<?php

class M_Daftar_menu extends CI_Model{

    protected $table = "menu";

    function create($data){
        $this->db->insert($this->table,$data);
    }

    function show(){
        $this->db->select('*');
		$this->db->from($this->table);
		$query = $this->db->get();
        return $query;
    }

    function show_where($where){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('menu_kode', $where);
		$query = $this->db->get();
        return $query;
    }

    function edit($where, $data)
	{
		$this->db->update($this->table, $data, $where);
    }
    
    function delete($where)
	{
		$this->db->where('menu_kode', $where);
		$this->db->delete($this->table);
    }

    function level_code()
	{
		$this->db->select('*');
		$this->db->from('admin_level');
		$query = $this->db->get();
        return $query;
    }


  

    
}